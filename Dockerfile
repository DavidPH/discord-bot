FROM python

WORKDIR /app

COPY requirements.txt ./
RUN pip install -U -r requirements.txt

CMD ["python", "run.py"]
