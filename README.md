# Duckbot

<img align="right" src="https://i.imgur.com/pudDCOz.png" width="20%">

[![Discord.py Version](https://img.shields.io/badge/discord.py-1.5.1-blue?style=flat)](https://discordpy.readthedocs.io/en/latest/intro.html)
[![Python Version](https://img.shields.io/badge/python-3.8-blue?style=flat)](https://www.python.org/downloads/)

**Outdated version** *see [DuckBot-v2](https://gitlab.com/DavidPH/duckbot-2.0)*

DuckBot is a discord bot written in python originally intended for personal use, but now used in a few different servers.

## Features

Key features:

  - Jail roles:
    - Replaces a user's roles with a pre-determined jail role. (e.g. member with role X in their roles will get them all removed and replaced with role Y). This can be used to easily mute members / or temporarily restrict their access.
    - Auto-Jail: Set up a list of banned words, that when said will cause the user to get jailed by the bot.
    - Bot remembers the user's old roles and will restore them automatically (if chosen) or on calling the `free` command.
    - You can set any amount of jail roles, and their priority. The bot will pick the appropriate jail role for the user and apply it to them.

  - Custom signups:

     - Create a signup message where users click a reaction to get their name added to it.
     - Can limit the amount of people who can register to an event, or make the event require the user to have a role.
     - Create signups with multiple messages and sections. Every section can have individual requirements / titles set. Bot will make sure there are no duplicate users registered to the same event.
     - Assign your custom signup a name, for easier future usage.
     - Don't want to worry about customizing a signup? The default one is applicable in most cases!

  - Fun commands:
     - DuckBot has a whole category with over 20 miscellaneous commands / games (e.g. "Rock Paper Scissors", "Trivia", "Meme maker").
     - You can disable any command you do not want, and add command cooldowns so that they are not spammed.

## How to run:

I would prefer if you didn't run a private instance of DuckBot, and [invite](https://discord.com/oauth2/authorize?client_id=592492103079297024&permissions=2081418481&scope=bot) the bot to your server instead, but if you absolutely *must* here is how to set  it up:<br>
Docker is the preferred (and easier) method, but if you're using Windows you will probably have to use a virtual environment.

#### Using Docker:
1.  **Install Docker Engine and docker-compose**

2. **Setup configuration**

    Edit `docker-compose.yml` and set the proper environment variables
    ````yaml
            environment:
            - TOKEN=yourtoken
            - DATABASE=duckbot:yourpass@db:5432
            - LATEST=channelid
            - FEEDBACK=channelid
    ````
   
3. **Run the bot!**

    Simply run `docker-compose up` to start up the database and the bot.

#### Virtual environment: 

1. **Install Python 3.8 or higher**

2. **Set up a virtual environment (venv)**

    Type `python3.8 -m venv venv` into the console.

3. **Install dependencies**
   
    You must have pip installed, then run `pip install -U -r requirements.txt`

4. **Create the database in PostgreSQL**

    The bot runs on PostgreSQL 12.3, but you can use 9.5 onwards.<br>Start up a PostgreSQL server on your machine  and then run the following commands in the `psql` tool.
    ````postgresql
    CREATE ROLE duckbot WITH LOGIN PASSWORD 'yourpw';
    CREATE DATABASE duckbot OWNER duckbot;
    ````

5. **Setup configuration**

    Edit the `bot.config` file or pass in the appropriate flags when starting up the bot. If you want to avail of the `meme` command then edit `fun.config` and modify `imgflip_u` and `imgflip_p` with your imgflip username and password.

   To set up the bot without manually editing `bot.config` type the following without quotation marks

   `python3.8 run.py -t 'discord api token' -db 'duckbot:yourpw@host:port' -l 'id of latest channel' -f 'id of feedback channel' -s`

6. **Run the bot!**
    
   To start up the bot, once everything has been configured is as simple as typing `python3.8 run.py`


## Support Server

Please click [**here**](https://discord.gg/XZkUPTD) if you would like to join the support server for any questions / issues / or feedback you would like to talk about.
