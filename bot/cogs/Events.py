import re
import json
import discord as d
from discord.ext import commands
from asyncio import TimeoutError
from asyncpg import UniqueViolationError
import typing


class SignUp:
    def __init__(self, raw_format=None, members=None):
        self.raw_format = raw_format
        self.format = {"messages": 1,
                       "titles": [],
                       "sections": [],
                       "limits": [],
                       "reactions": [],
                       "roles": [],
                       "requires": [],
                       "inline": False,
                       "members": members
                       }
        if raw_format:
            self.parse_format(raw_format)

    def make_mem_lst(self):
        return [[[] for _ in range(len(self.format["sections"]) or 1)] for _ in
                range(self.format["messages"])]

    def parse_format(self, raw_format):
        rgx = r'(?<=\{).+?(?=\})'
        s = re.findall(rgx, raw_format)

        for match in s:
            if match.lower().replace(" ", "") == "inline":
                self.format["inline"] = True
            splt = re.split(r' *= *', match)
            if len(splt) < 2:
                continue
            key = splt[0].replace(" ", "")
            value = splt[1]
            if key == "messages":
                self.format[key] = int(value)
                continue
            elif key == "titles" or key == "sections" or key == "title":
                if key == "title":
                    key = "titles"
                rgx = r'"(?:[^"\\]|\\.)*"|\'(?:[^\'\\]|\\.)*\''
                titles = [x.group()[1:-1].rstrip() for x in re.finditer(rgx, value)]
                self.format[key] = titles
                continue
            elif key == "limits":
                for limit in value.replace(",", "").split(" "):
                    if limit:
                        self.format[key].append(int(limit))
                continue
            elif key == "reactions":
                value = value.split(" ")
                for emote in value:
                    if emote != " " and emote != "":
                        if emote not in self.format[key]:
                            self.format[key].append(emote)
                continue
            elif key == "roles":
                rgx = r'\d{17,21}'
                value = value.split("|")
                for val in value:
                    role_list = [int(x.group().rstrip()) for x in re.finditer(rgx, val)]
                    if role_list:
                        self.format[key].append(role_list)
                continue
            elif key == "requires":
                rgx = r'\d{17,21}'
                roles = [int(x.group().rstrip()) for x in re.finditer(rgx, value)]
                self.format[key] = roles
                continue

    def __str__(self):
        out = ""
        for key, value in self.format.items():
            if key == "members":
                continue
            if value:
                val = ""
                if key == "titles" or key == "sections":
                    for e in value:
                        val += f"\"{e}\" "
                    val = val[:-1]
                elif key == "roles":
                    for sl in value:
                        for e in sl:
                            val += f"{e} "
                        val += "| "
                    val = val[:-3]
                elif type(value) == list:
                    for e in value:
                        val += f"{e} "
                    val = val[:-1]
                else:
                    val = value
                if key == "inline":
                    out += f"{{{key}}}\n"
                else:
                    out += f"{{ {key} = {val} }}\n"

        return out


def perm_check():
    async def pred(ctx):
        if ctx.author.guild_permissions.administrator:
            return True
        async with ctx.bot.pool.acquire() as con:
            server_perms = await con.fetch("SELECT snowflake_id, jail, signup FROM permissions "
                                           "WHERE server_id = $1", ctx.guild.id)

        allowed = False
        for record in server_perms:
            if record["snowflake_id"] == ctx.author.id:
                if record["signup"]:
                    allowed = True
                elif record["signup"] is not None:
                    return False
            else:
                role = ctx.guild.get_role(record["snowflake_id"])
                if role:
                    if role in ctx.author.roles:
                        if record["signup"]:
                            allowed = True
                        elif record["signup"] is not None:
                            return False
        return allowed

    return commands.check(pred)


class Events(commands.Cog):
    """
        Very customizable, easy to use signups for events.
        Maximum 20 signups per server, oldest one is replaced on signup creation if over the limit.

        See {prefix}help signup for a list of format options to create a custom signup.

    """

    def __init__(self, client):
        self.client = client
        self.color = 0xfef200
        self.empty = "\u200b"
        self.default_reaction = "\u2705"

    def make_embed(self, p_format, title, members=None):
        embed = d.Embed(color=self.color)
        if not members:
            members = []
        val = ""
        inline = p_format["inline"]
        t_total = 0
        l_total = 0
        for i, section in enumerate(p_format["sections"]):
            s_total = f"({len(members[i])})"
            t_total += len(members[i])
            if l_total >= 0:
                if p_format["limits"][i] > 0:
                    l_total += p_format["limits"][i]
                else:
                    l_total = -1
            if p_format["limits"][i] > 0:
                s_total = s_total[:-1] + f"/{p_format['limits'][i]})"
            section = section.replace("[total]", s_total)
            if inline:
                val = ""

            if not inline:
                val += section + bool(section) * "\n"
            val += "```\n"
            if i >= len(members) or not members[i]:
                val += self.empty
                if inline:
                    val += " " * 13
            elif i < len(members):
                for member in members[i]:
                    val += f"+ {member}"
                    val += "\n"
            if i == len(p_format["sections"]) - 1:
                if not inline:
                    val += " " * 35
            val += "```\n"
            if inline:
                embed.add_field(name=self.empty if not section else section, value=val)

        if title:
            t_total = f"({t_total})"
            if l_total > 0:
                t_total = t_total[:-1] + f"/{l_total})"
            title = title.replace("[total]", str(t_total))
            if inline:
                embed.description = title
            else:
                embed.title = title
        if not inline:
            embed.description = val
        return embed

    # Return (True, SignUp) if the raw format is valid
    # Return (False, error message) if the raw format is invalid
    def validate_format(self, ctx, raw_format):
        try:
            signup = SignUp(raw_format)
        except:
            return False, "Couldn't parse signup message"

        # Missing input
        if not signup.format["sections"]:
            signup.format["sections"].append("")
        if not signup.format["reactions"]:
            signup.format["reactions"].append(self.default_reaction)
        if not signup.format["titles"]:
            signup.format["titles"].append("")
        while len(signup.format["limits"]) < len(signup.format["sections"]):
            signup.format["limits"].append(-1)
        while len(signup.format["titles"]) > signup.format["messages"]:
            signup.format["messages"] += 1

        signup.format["members"] = signup.make_mem_lst()
        # Validating
        output = ""
        for role_list in (signup.format["roles"], signup.format["requires"]):
            for item in role_list:
                if type(item) == list:
                    for role in item:
                        if not ctx.guild.get_role(role):
                            output += f"+ Invalid role ID: {role}.\n"
                else:
                    if not ctx.guild.get_role(item):
                        output += f"+ Invalid role ID: {item}.\n"

        # More than 4 messages
        if signup.format["messages"] > 5:
            output += "+ Maximum messages per signup is 5.\n"
        # More reactions than sections -> invalid format
        if len(signup.format["reactions"]) > 1 and len(signup.format["reactions"]) > len(signup.format["sections"]):
            output += "+ Multiple reactions without their corresponding sections.\n" \
                      "  (Use 1 reaction, or same amount of reactions and sections)\n"
        # Number of titles must equal number of messages
        if len(signup.format["titles"]) > signup.format["messages"]:
            output += f"+ Used {len(signup.format['titles'])} titles with {signup.format['messages']} messages.\n" \
                      f"  Each message can only have 1 title (or none).\n"
        # More limits than sections
        if len(signup.format["limits"]) > len(signup.format["sections"]):
            output += "+ More limits than sections used. Each section can only have 1 limit, or no limit (blank).\n"
        # Multiple roles and multiple reactions
        if len(signup.format["roles"]) > 0 and len(signup.format["reactions"]) > 1:
            output += "+ Sections can be signed onto by 1 reaction and multiple specified roles (using {roles = }, " \
                      "or by multiple reactions and no roles.\n" \
                      "  You can make the signup locked to one or more roles by using {requires = role(s)}\n"
        # More role sections than sections
        if len(signup.format["roles"]) > len(signup.format["sections"]):
            output += f"+ When specifying roles divide the sections with \" | \".\n" \
                      f"Currently there are {len(signup.format['sections'])} sections, " \
                      f"but you tried to specify roles for {len(signup.format['roles'])} sections."

        if output != "":
            return False, output
        return True, signup

    async def send_signup(self, ctx, signup):
        msg_ids = []
        for i in range(signup.format["messages"]):
            title = "" if i >= len(signup.format["titles"]) else signup.format["titles"][i]
            msg = await ctx.send(
                embed=self.make_embed(signup.format, title, signup.format["members"][i]))
            for reaction in signup.format["reactions"]:
                try:
                    await msg.add_reaction(reaction)
                except d.HTTPException:
                    await ctx.send(f"Couldn't find emote: {reaction} make sure I'm in the same server with that emote.")
                    return
            msg_ids.append(msg.id)
        return msg_ids

    @commands.guild_only()
    @commands.group(invoke_without_command=True, name="signup", usage="<format>|<format_name>")
    @perm_check()
    async def signup(self, ctx, *, r_format=None):
        """
        Creates a signup, with an optional format.
        [format_name] is the name chosen by the `signup save` command
        [format] takes the form of one or multiple expressions surrounded by { }
        A user can only register once per signup. (i.e. no duplicates will be in the signup)

        **Valid expressions:**
        ```Nginx
        {messages = X}
        ``` Where X is the amount of messages in the signup.
            All these messages count as one signup.
            If multiple messages, then every message shares the same properties, other than the title.

        ```Nginx
        {title = "msg1_title" "msg2_title" ... "msgN_title"}
        ``` The title of each message.
            For no title use "" or ommit this option.

        ```Nginx
        {sections = "section1" "section2" ... "sectionN"}
        ``` The sections in each message.
            For no name use ""

        ```Nginx
        {limits = s1_limit s2_limit ... sN_limit}
        ``` Number of members who can sign up for each section.
            For no limit ommit this option or use -1 e.g {limits = 10 -1 5}
            If limits are set, and no section reactions are set then the bot will try fill from top to bottom.

        ```Nginx
        {reactions = s1_reaction s2_reaction ... sN_reaction}
        ``` Reacting with this emote will sign up the user to the corresponding section.

        ```Nginx
        {requires = role1 role2 .... roleN}
        ``` Makes it so that users must have these roles to sign up.

        ```fix
        {inline}
        ``` The sections will display side by side, instead of stacked on top of each other.

        """
        async with self.client.pool.acquire() as con:
            p_format = await con.fetchval("SELECT format FROM signup_settings "
                                          "WHERE server_id = $1 AND format_name = $2", ctx.guild.id, r_format)
            if not p_format:
                valid, output = self.validate_format(ctx, r_format)
                if not valid:
                    embed = d.Embed(color=self.color)
                    embed.set_footer(text=f"For help type {ctx.prefix}help signup")
                    embed.add_field(name="Invalid Format", value=output)
                    await ctx.send(embed=embed)
                    return

                signup = output
            else:
                signup = SignUp()
                signup.format = json.loads(p_format)
            msg_ids = await self.send_signup(ctx, signup)
            if not msg_ids:
                return

            async with con.transaction():
                await con.execute("""
                        DELETE FROM signups 
                        WHERE (SELECT COUNT(*) FROM signups) >= 20
                        AND
                        server_id=$1
                        AND
                        timestamp IN (SELECT timestamp FROM signups ORDER BY timestamp ASC LIMIT 1)""", ctx.guild.id)

                await con.execute("INSERT INTO signups VALUES ($1, $2, $3)",
                                  ctx.guild.id, msg_ids, json.dumps(signup.format))
        if r_format and not p_format:
            try:
                await ctx.message.author.send("If you want to save this format with a name "
                                              "for easier use type (in the server):"
                                              f"```\n{ctx.prefix}signup save {r_format}```\n")
            except d.Forbidden:
                pass
        await ctx.message.delete()

    @commands.guild_only()
    @signup.command(usage="<format>")
    @perm_check()
    async def save(self, ctx, *, r_format=None):
        """
        Saves a <format> to a keyword.
        To see how to make a <format> check {prefix}help signup.
        """
        valid, output = self.validate_format(ctx, r_format)

        embed = d.Embed(color=self.color)
        embed.set_footer(text=f"For help type {ctx.prefix}help signup")
        if not r_format:
            embed.add_field(name="Error",
                            value="Missing format.\n"
                                  f"`{ctx.prefix}signup save <format>`")
            await ctx.send(embed=embed)
            return
        if not valid:
            embed.add_field(name="Invalid Format", value=output)
            await ctx.send(embed=embed)
            return

        await ctx.send("Preview of signup: ")
        msg_ids = await self.send_signup(ctx, output)
        if not msg_ids:
            return
        reserved_names = ["save", "delete", "formats", "examples", "cancel"]
        await ctx.send(
            f"Type a name (no spaces) to give to this signup, you will then be able to post it using "
            f"`{ctx.prefix}signup name`\n"
            "To cancel type \"cancel\"")
        try:
            while True:
                reply = await self.client.wait_for('message', check=lambda x: x.author == ctx.message.author,
                                                   timeout=25)
                msg = reply.content.split(" ")[0]
                if msg.lower() == "cancel":
                    await ctx.send("Cancelled.")
                    return
                if msg.lower() in reserved_names:
                    await ctx.send("Sorry can't use that name. Try another name: ")
                    continue
                break
        except TimeoutError:
            await ctx.send("Cancelled (timed out)")
            return

        async with self.client.pool.acquire() as con:
            async with con.transaction():
                try:
                    await con.execute("INSERT INTO signup_settings "
                                      "VALUES ($1, $2, $3)", ctx.guild.id, msg, json.dumps(output.format))
                except UniqueViolationError:
                    embed.add_field(name="**A signup with that name already exists in this server.**",
                                    value=f"To view all the saved signups type `{ctx.prefix}signup formats`\n"
                                          f"To delete this format type `{ctx.prefix}signup delete {msg}`")
                    await ctx.send(embed=embed)
                    return
        embed.title = f"Saved format with name \"{msg}\""
        embed.add_field(name="Format:", value=f"```Nginx\n{output}\n```\n "
                                              f"**Type `{ctx.prefix}signup {msg}` to post a signup using this format.**")

        await ctx.send(embed=embed)

    @commands.guild_only()
    @signup.command(usage="<format_name>")
    @perm_check()
    async def delete(self, ctx, name):
        """
            Deletes a saved format.
        """
        async with self.client.pool.acquire() as con:
            async with con.transaction():
                rc = await con.execute("DELETE FROM signup_settings "
                                       "WHERE server_id = $1 AND format_name = $2", ctx.guild.id, name)

        if rc == "DELETE 1":
            await ctx.send(f"Deleted format \"{name}\".")
        else:
            await ctx.send(f"Couldn't find format with name \"{name}\"\n"
                           f"Type `{ctx.prefix}signup formats` to view a list of all saved formats.")

    @commands.guild_only()
    @signup.command()
    @perm_check()
    async def formats(self, ctx):
        """
        Shows the saved formats.
        """

        embed = d.Embed(color=self.color)
        embed.set_author(name=self.client.user.name, icon_url=self.client.user.avatar_url)

        async with self.client.pool.acquire() as con:
            pairs = await con.fetch("SELECT format_name, format "
                                    "FROM signup_settings WHERE server_id = $1", ctx.guild.id)

        if not pairs:
            embed.add_field(name="**No formats have been saved.**",
                            value=f"To save a format use `{ctx.prefix}signup save <format>`")
        else:
            for record in pairs:
                s = SignUp()
                s.format = json.loads(record["format"])
                embed.add_field(name=f"**{ctx.prefix}signup {record['format_name']}**",
                                value=f"```Nginx\n{s}```", inline=False)
        embed.set_footer(text=f"For a full set of help commands and permissions type {ctx.prefix}help")
        await ctx.send(embed=embed)

    @signup.command(usage="<member|role>")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def allow(self, ctx, arg: typing.Union[d.Role, d.Member]):
        """
        Gives permissions to use signup.
        """
        func = self.client.get_command("allow")
        await func(ctx, arg)

    @signup.command(usage="<member|role>")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def deny(self, ctx, arg: typing.Union[d.Role, d.Member]):
        """
        Removes permissions to use signup.
        This does not work as a blacklist, it will only remove existing permissions.
        """
        func = self.client.get_command("deny")
        await func(ctx, arg)

    @signup.command()
    @commands.guild_only()
    async def perms(self, ctx):
        """
        Member/roles who can use signup.
        This does not include bot-wide permissions.
       """
        func = self.client.get_command("perms")
        await func(ctx)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, event):
        server = self.client.get_guild(event.guild_id)
        if not server:
            return
        user = server.get_member(event.user_id)
        if user == self.client.user:
            return
        async with self.client.pool.acquire() as con:
            result = await con.fetch("SELECT message_id, content FROM signups WHERE server_id = $1", server.id)
            for record in result:
                msg_ids = record["message_id"]
                try:
                    msg_index = msg_ids.index(event.message_id)
                    p_format = json.loads(record["content"])
                    reaction_index = p_format["reactions"].index(event.emoji.name)  # Section index = emote index
                    section_index = reaction_index
                except ValueError:
                    continue
                if p_format["requires"]:
                    for role_id in p_format["requires"]:
                        role = server.get_role(role_id)
                        if role in user.roles:
                            break
                    else:
                        return
                for i in range(len(p_format["roles"])):
                    for role_id in p_format["roles"][i]:
                        role = server.get_role(role_id)
                        if role in user.roles:
                            if p_format["limits"][i] < 0:
                                section_index = i
                                break
                            else:
                                if len(p_format["members"][msg_index][i]) < p_format["limits"][i]:
                                    section_index = i
                                    break
                    else:
                        continue

                    break
                else:
                    if p_format["roles"]:
                        if len(p_format["sections"]) > len(p_format["roles"]):
                            section_index = len(p_format["roles"])
                        else:
                            return
                    else:
                        if len(p_format["sections"]) > len(p_format["reactions"]):
                            if reaction_index == len(p_format["reactions"]) - 1:
                                for i in range(section_index, len(p_format["sections"])):
                                    if p_format["limits"][i] > 0:
                                        if len(p_format["members"][msg_index][i]) < p_format["limits"][i]:
                                            section_index = i
                                            break
                                    else:
                                        section_index = i

                for mem_list in p_format["members"]:
                    if any(event.user_id in sl for sl in mem_list):
                        return
                for i, mem_list in enumerate(p_format["members"]):
                    if i == msg_index:
                        if p_format["limits"][section_index] > 0:
                            if len(mem_list[section_index]) >= p_format["limits"][section_index]:
                                try:
                                    await user.send(
                                        f"The section you tried to register for is already full "
                                        f"({p_format['limits'][section_index]} / {p_format['limits'][section_index]})")
                                except d.Forbidden:
                                    pass
                                return
                        mem_list[section_index].append(event.user_id)

                        async with con.transaction():
                            await con.execute(
                                "UPDATE signups SET content = $1 WHERE server_id = $2 AND message_id = $3",
                                json.dumps(p_format), event.guild_id, record["message_id"])
                        embed = self.make_embed(p_format, p_format["titles"][msg_index],
                                                [[server.get_member(mem_id).display_name if
                                                  server.get_member(mem_id) else mem_id for mem_id in sl] for
                                                 sl in p_format["members"][msg_index]])
                        channel = server.get_channel(event.channel_id)
                        msg = await channel.fetch_message(msg_ids[msg_index])
                        await msg.edit(embed=embed)

                        return

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, event):
        user = self.client.get_user(event.user_id)
        if user == self.client.user:
            return
        server = self.client.get_guild(event.guild_id)
        if not server:
            return
        async with self.client.pool.acquire() as con:
            result = await con.fetch("SELECT message_id, content FROM signups WHERE server_id = $1", server.id)
            for record in result:
                msg_ids = record["message_id"]
                try:
                    msg_index = msg_ids.index(event.message_id)
                    p_format = json.loads(record["content"])
                    p_format["reactions"].index(event.emoji.name)
                except ValueError:
                    continue

                for i, mem_list in enumerate(p_format["members"]):
                    if i == msg_index:
                        for sl in mem_list:
                            for mem_id in sl:
                                if mem_id == event.user_id:
                                    sl.remove(mem_id)
                                    new_list = [[server.get_member(mem_id).display_name if
                                                 server.get_member(mem_id) else mem_id for mem_id in sl] for
                                                sl in p_format["members"][msg_index]]

                                    async with con.transaction():
                                        await con.execute(
                                            "UPDATE signups SET content = $1 WHERE server_id = $2 AND message_id = $3",
                                            json.dumps(p_format), event.guild_id, record["message_id"])
                                    embed = self.make_embed(p_format, p_format["titles"][msg_index], new_list)
                                    channel = server.get_channel(event.channel_id)
                                    msg = await channel.fetch_message(msg_ids[msg_index])
                                    await msg.edit(embed=embed)
                                    return


def setup(client):
    signup_cog = Events(client)
    client.add_cog(signup_cog)
