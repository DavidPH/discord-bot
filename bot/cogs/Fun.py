import random
import json
import discord as d
from discord.ext import commands
import configparser
from io import BytesIO
from PIL import Image
import typing
import asyncio
import re
from datetime import datetime
import html
from sys import maxsize as max_int_size


class Fun(commands.Cog):
    """
    Miscellaneous commands & games.
    """

    def __init__(self, client):
        self.client = client
        self.color = 0xfef200
        self.session = self.client.session
        self.config = configparser.ConfigParser()
        self.config.read(self.client.dirpath / "resources/config/fun.config", encoding="utf-8")
        self.meme_list = {"checked": None, "memes": None}
        self.trivia_categs = {"checked": None, "categs": None}
        self.trivia_tokens = {}
        self.on_going_trivia = {}
        self.disabled = {}

    def cog_check(self, ctx):
        if ctx.guild is None:
            return False
        if ctx.author.guild_permissions.administrator:
            return True
        if ctx.command.name == "togglefun":
            raise commands.CommandNotFound

        if ctx.guild.id in self.disabled:
            if self.disabled[ctx.guild.id]:
                raise commands.CommandNotFound
        return True

    def default_embed(self):
        return d.Embed(color=self.color)

    @commands.command(brief="Toggle Fun Commands")
    async def togglefun(self, ctx):
        """
        Temporary experimental toggle to disable of entire Fun commands.
        A more modular and better working version will be available in 2.0
        """
        if ctx.guild.id not in self.disabled:
            self.disabled[ctx.guild.id] = True
        else:
            self.disabled[ctx.guild.id] = not self.disabled[ctx.guild.id]
        await ctx.send(f"Fun module {'disabled' if self.disabled[ctx.guild.id] else 'enabled'}")

    @commands.command(brief="Random piece of advice")
    async def advice(self, ctx):
        try:
            async with self.session.get(self.config["API"]["advice"]) as r:
                res = await r.read()
                res = json.loads(res)
                embed = self.default_embed()
                embed.description = res['slip']["advice"]
                await ctx.send(embed=embed)
        except Exception:
            await ctx.send("Sorry I can't give you any advice right now :(")

    @commands.command(brief="Give someone a beer!", usage="<member>")
    async def beer(self, ctx, user: d.Member = None, *, reason=None):
        beer_l = "<:beerL:724868607171100692>"
        beer_r = "<:beerR:724868622580973588>"
        if not user or user == ctx.author:
            await ctx.send(f"Alone? Aww ;-; I'll share a beer with you {beer_l}")
        elif user == ctx.guild.me:
            await ctx.send(f"Thanks for the beer, **{ctx.author.display_name}** {beer_l}")
        else:
            if reason:
                reason = "Reason: " + reason + "\n"
            else:
                reason = ""
            await ctx.send(f"**{user.display_name}**, you got a \uD83C\uDF7A from **{ctx.author.display_name}**\n\n"
                           + reason + f"{beer_l}{beer_r}")

    # @commands.command(brief="Cute birbs")
    # async def birb(self, ctx):
    #     try:
    #         async with self.session.get(self.config["API"]["birb"]) as r:
    #             res = await r.json()
    #             embed = self.default_embed()
    #             embed.set_image(url=res['file'])
    #             await ctx.send(embed=embed)
    #     except Exception:
    #         await ctx.send("I couldn't find any birbs :(")

    @commands.command(brief="Throw a boot at someone", usage="<member>")
    async def boot(self, ctx, user: d.Member = None):
        if not user:
            await ctx.send("Who are you throwing that at?")
            return

        quotes = json.loads(self.config["REPLIES"]["boot"])
        await ctx.send(random.choice(quotes).format(ctx.author.display_name, user.display_name))

    @commands.command(brief="Cat.")
    async def cat(self, ctx):
        try:
            async with self.session.get(self.config["API"]["cat"]) as r:
                res = await r.json()
                embed = self.default_embed()
                embed.set_image(url=res['url'])
                await ctx.send(embed=embed)
        except Exception:
            await ctx.send("I couldn't find any cats :(")

    @commands.command(brief="Picks from a list of choices", usage="[<choice> | <choice> ...]")
    async def choose(self, ctx, *, message=None):
        if not message:
            await ctx.send("Choose what..?")
            return
        message = message.split("|")
        choice = random.choice(message)
        choice = random.choice(json.loads(self.config['REPLIES']['choose'])).format(choice)
        await ctx.send(f"**{ctx.author.display_name}**, {choice}")

    @commands.command(brief="Flip a coin", aliases=["flip", "coin"])
    async def coinflip(self, ctx):
        result = random.choice(["Heads", "Tails"])
        await ctx.send(f"**{ctx.author.display_name}** flipped a coin and got **{result}**!")

    @commands.command(brief="Give someone a cookie!", usage="<member>")
    async def cookie(self, ctx, user: d.Member = None, *, reason=None):
        if not user:
            await ctx.send("Who are you giving that to?")
            return
        out = f"**{user.display_name}**, you got a \uD83C\uDF6A from **{ctx.author.display_name}**\n\n"
        if reason:
            out += f"**Reason:** {reason}\n"
        out += "(ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ \uD83C\uDF6A"
        await ctx.send(out)

    # I don't always tell dad jokes... But when I do he laughs
    @commands.command(brief="Tell a dad joke")
    async def dadjoke(self, ctx):
        try:
            async with self.session.get(self.config["API"]["dadjoke"], headers={'Accept': 'application/json'}) as r:
                res = await r.json()
                embed = self.default_embed()
                embed.description = res["joke"]
                await ctx.send(embed=embed)
        except Exception:
            await ctx.send("I'm all out of jokes :(")

    @commands.command(brief="Cute doggos")
    async def dog(self, ctx):
        try:
            async with self.session.get(self.config["API"]["dog"]) as r:
                res = await r.json()
                res = res["url"]
                if res[-4:] != ".mp4":
                    embed = self.default_embed()
                    embed.set_image(url=res)
                    await ctx.send(embed=embed)
                else:
                    await ctx.send(res)
        except Exception:
            await ctx.send("I couldn't find any dogs :(")

    @commands.command(brief="Cute duckos")
    async def duck(self, ctx):
        try:
            async with self.session.get(self.config["API"]["duck"]) as r:
                res = await r.json()
                embed = self.default_embed()
                embed.set_image(url=res["url"])
                await ctx.send(embed=embed)
        except Exception:
            await ctx.send("Sorry I couldn't find any ducks :(")

    @commands.command(brief="Ask 8ball a question", name="8ball", aliases=["eightball"], usage="<question>")
    async def eightball(self, ctx, *, question=None):
        if not question:
            await ctx.send("You need to ask something")
            return
        reply = random.choice(json.loads(self.config["REPLIES"]["8ball"]))
        await ctx.send(f"\uD83C\uDFB1 **Question:** {question}\n**Answer:** {reply}")

    @commands.command(brief="Press F to pay respects", name="f", usage="[reason]")
    async def respects(self, ctx, *, reason=None):
        heart = random.choice(["\u2764", "\uD83D\uDC9B", "\uD83D\uDC9A", "\uD83D\uDC99", "\uD83D\uDC9C"])
        await ctx.send(
            f"**{ctx.author.display_name}** has paid their respects " + (
                f"for **{reason}** " if reason else "") + heart)

    @commands.command(brief="Give someone a flower", usage="<member>")
    async def flower(self, ctx, user: d.Member = None):
        flower = random.choice(
            ["\uD83C\uDF37", "\uD83C\uDF3C", "\uD83C\uDF38", "\uD83C\uDF3A", "\uD83C\uDF3B", "\uD83C\uDF39"])
        if not user:
            await ctx.send("Who are you giving that flower to..?")
            return

        if user == ctx.guild.me:
            await ctx.send("*Awww*")
        else:
            await ctx.send(f"**{user.display_name}** you got a {flower} from **{ctx.author.display_name}**")

    @commands.command(brief="Give someone a fruit", usage="[member]")
    async def fruit(self, ctx, user: d.Member = None):
        fruit = random.choice(json.loads(self.config["REPLIES"]["fruits"]))

        if not user:
            await ctx.send(
                f"**{ctx.author.display_name}** drops a fruit."
                f"\n\n{fruit}\n ლ༼ ◕ _ ◕ ༽ What a waste...")
            return
        await ctx.send(f"**{user.display_name}** you got a {fruit} from **{ctx.author.display_name}**\n\n"
                       f"(ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ {fruit}")

    @commands.command(brief="Did you know?", aliases=["fact"])
    async def funfact(self, ctx):
        try:
            async with self.session.get(self.config["API"]["funfact"]) as r:
                res = await r.json()
                embed = self.default_embed()
                embed.description = res["text"]
                await ctx.send(embed=embed)
        except Exception:
            await ctx.send("I couldn't find any facts :(")

    @commands.command(brief="Generate an insult", usage="[member]")
    async def insult(self, ctx, user: d.Member = None):
        try:
            async with self.session.get(self.config["API"]["insult"]) as r:
                res = await r.text()
                res = html.unescape(res)
                if user:
                    await ctx.send(f"{user.mention} {res}")
                else:
                    await ctx.send(res)
        except Exception:
            await ctx.send("Coudln't generate an insult.")

    async def get_meme_list(self, ctx):
        elapsed = False
        if self.meme_list["checked"]:
            t = datetime.now() - self.meme_list["checked"]
            if t.seconds / 60 >= 30:
                elapsed = True
        if elapsed or not self.meme_list["checked"] or not self.meme_list["memes"]:
            try:
                async with self.session.get(self.config["API"]["imgflip"]) as r:
                    res = await r.json()
                    if res["success"]:
                        memes = res["data"]["memes"]
                    else:
                        raise RuntimeError
                    memes = sorted(memes, key=lambda x: x["name"])
                    self.meme_list = {"checked": datetime.now(), "memes": memes}
                    return memes
            except Exception:
                await ctx.send("Error connecting to imgflip API")
        else:
            return self.meme_list["memes"]

    async def make_meme(self, template, text):
        meme_id = template["id"]
        box_count = int(template["box_count"])
        n_template = {
            "username": self.config["API"]["imgflip_u"],
            "password": self.config["API"]["imgflip_p"],
            "template_id": meme_id,
        }
        for i in range(box_count):
            if i < len(text):
                n_template[f"boxes[{i}][text]"] = text[i] or " "
            else:
                n_template[f"boxes[{i}][text]"] = " "
        async with self.session.post(self.config["API"]["imgflip_post"], data=n_template) as resp:
            res = await resp.json()
            if not res["success"]:
                raise ValueError(res["error_message"])
            else:
                return res["data"]["url"]

    @commands.group(invoke_without_command=True, case_insensitive=True, usage="<meme_id | member> <text>")
    async def meme(self, ctx, meme: typing.Union[d.Member, int], *, text: commands.clean_content):
        """
        Make a custom meme.
        If a user is tagged, it will use the user's profile picture as the meme image.
        Else a meme ID (gotten from `{prefix}meme list`) must be given.
        The meme <text> is separated with " | " e.g. top_text | bottom_text
        Amount of text boxes available for a given meme is also gotten from `meme list`
        """
        if type(meme) == d.Member:
            await self.usermeme(ctx, user=meme, text=text)
            return
        text = str(text).split("|")
        lst = await self.get_meme_list(ctx)
        meme -= 1
        if meme < 0 or meme > len(lst) - 1:
            raise commands.UserInputError
        meme = lst[meme]
        embed = self.default_embed()
        try:
            # next(item for item in lst if item["name"] == "Distracted Boyfriend")
            url = await self.make_meme(meme, text)
        except Exception as e:
            embed.description = "**Error**\n" + str(e)
        else:
            embed.set_image(url=url)
            embed.set_footer(text=f"To see a list of available memes type {ctx.prefix}meme list")

        await ctx.send(embed=embed)

    @meme.command(name="list")
    async def meme_list(self, ctx):
        """
        Shows a list of available memes.
        """
        lst = await self.get_meme_list(ctx)
        paginator = commands.Paginator(max_size=1550, prefix="```css\n[ID]  Name (number of text boxes)\n")
        for i, meme in enumerate(lst):
            paginator.add_line(f"[{i+1}]{' '* (3 - (len(str(i+1)) -1))}{meme['name']} ({meme['box_count']})")
        for i, page in enumerate(paginator.pages):
            embed = self.default_embed()
            embed.description = page
            embed.set_footer(text=f"({i + 1} / {len(paginator.pages)}) "
                                  f"To make a meme use {ctx.prefix}meme <meme_id> text1 | text2 ...")
            if i == len(paginator.pages) - 1:
                embed.description += "[Source Images](https://imgflip.com/memetemplates)\n" \
                                     f"Type `{ctx.prefix}meme info <id>` to see a template.\n" \
                                     f"Or you can also search for one using `{ctx.prefix}meme info <search_term>`"
            try:
                await ctx.author.send(embed=embed)
            except d.Forbidden:
                await ctx.send("Couldn't DM you the list, check if you have me blocked or if you have disabled DMs.")
                return
            await ctx.message.add_reaction("\U00002709")

    @meme.command(name="info", usage="<meme_id | meme_name>")
    async def meme_info(self, ctx, search: typing.Union[int, str]):
        """
        Look up a meme.
        You can view a specific meme using the ID gotten from `{prefix}meme list`
        Or you can search for one by typing the name / part of the name.
        """
        lst = await self.get_meme_list(ctx)
        memes = []
        if type(search) == int:
            search -= 1
            if 0 <= search < len(lst):
                memes.append(lst[search])
        else:
            if len(search) < 2:
                await ctx.send("Too broad of a search term. Try again.")
                return
            for meme in lst:
                if search.lower() in meme["name"].lower():
                    memes.append(meme)
        embed = self.default_embed()
        if not memes:
            embed.description = f"**Error**\nMeme not found.\n" \
                                f"See `{ctx.prefix}meme list` for a full list of available memes."
        else:

            for meme in memes:
                meme_id = lst.index(meme) + 1
                template = f"Example: `{ctx.prefix}meme {meme_id} " \
                           f"{' | '.join([f'text{i+1}' for i in range(meme['box_count'])])}`"
                if len(memes) == 1:
                    embed.description = f"**[{meme_id}] {meme['name']}**\n" \
                                        f"Text boxes: {meme['box_count']}\n" \
                                        f"{template}"
                    embed.set_image(url=memes[0]["url"])
                else:
                    embed.add_field(name=f"[{meme_id}] {meme['name']}",
                                    value=f"Template: {meme['url']}\n"
                                          f"Text boxes: {meme['box_count']}\n"
                                          f"{template}"
                                    ,
                                    inline=False)

        await ctx.send(embed=embed)

    # @commands.command(brief="Make a meme with someone's picture", usage="<member> <upper_text> | <lower_text>")
    async def usermeme(self, ctx, user: d.Member = None, *, text=None):
        if not user:
            user = ctx.author
        if not text:
            await ctx.send("You must give top text, and bottom text separated by |")
            return
        text = text.split("|")
        if len(text) != 2:
            await ctx.send("Invalid arguments.\n e.g of valid arguments: \"TOP TEXT | BOTTOM TEXT\"")
            return

        def process(b):
            with Image.open(BytesIO(b)) as meme_img:
                o = BytesIO()
                meme_img.save(o, "png")
                o.seek(0)
                return o

        escape = {"-": "--", "_": "__", " ": "_", "?": "~q", "%": "~p", "#": "~h", "/": "~s", "\"": "''"}
        for i in range(2):
            for key in escape:
                text[i] = text[i].replace(key, escape[key])
        top_text, bottom_text = text
        async with ctx.typing():
            try:
                async with self.session.get(
                        self.config["API"]["meme"].format(top_text, bottom_text, user.avatar_url)) as r:
                    res = await r.read()

                output_buffer = await self.client.loop.run_in_executor(None, lambda: process(res))
                await ctx.send(file=d.File(fp=output_buffer, filename=f"{user.id}_meme.png"))
            except Exception:
                await ctx.send("I couldn't make the meme :(")

    @commands.command(usage="<text>")
    async def scroll(self, ctx, *, text: commands.clean_content):
        """
        Scroll of Truth meme
        Shortcut for `{prefix}meme 78 <text>`
        """
        lst = await self.get_meme_list(ctx)
        index = next((i + 1 for i, item in enumerate(lst) if item["name"] == "The Scroll Of Truth"), None)
        await self.meme(ctx, index, text=text)

    @commands.command(usage="<text>")
    async def drake(self, ctx, *, text: commands.clean_content):
        """
        Drake Hotline Bling meme
        Shortcut for `{prefix}meme 21 <text>`
        """
        lst = await self.get_meme_list(ctx)
        index = next((i + 1 for i, item in enumerate(lst) if item["name"] == "Drake Hotline Bling"), None)
        await self.meme(ctx, index, text=text)

    @commands.command(brief="Find out your pickle size!", usage="[member]")
    async def pickle(self, ctx, user: d.Member = None):
        if not user:
            user = ctx.author
        random.seed(user.id)
        size = random.randint(0, 50) / 1.17
        if user == ctx.author:
            await ctx.send(f"**{user.display_name}**, your pickle size is **{size:0.2f}cm** \uD83C\uDF80")
        else:
            await ctx.send(f"**{user.display_name}'s** pickle size is **{size:0.2f}cm** \uD83C\uDF80")

    @commands.command(brief="Rate something", usage="<message>")
    async def rate(self, ctx, *, message: commands.clean_content):
        p_bar = ["<:pb_l_e:726137285103583232>", "<:pb_l_f:726148987593162892>", "<:pb_m_e:726137284793204737>",
                 "<:pb_m_f:726147272139735080>", "<:pb_r_e:726137284453466183>", "<:pb_r_f:726149803540480120>"]

        embed = self.default_embed()
        score = random.randint(0, 100)
        filled_progbar = round(score / 100 * 10)
        if filled_progbar <= 0:
            counter = p_bar[0] + 8 * p_bar[2] + p_bar[4]
        elif filled_progbar >= 10:
            counter = p_bar[1] + 8 * p_bar[3] + p_bar[5]
        else:
            counter = p_bar[1] + p_bar[3] * (filled_progbar - 1) + (p_bar[2] * (9 - filled_progbar)) + p_bar[4]

        embed.add_field(name=f"I'd rate `{message}` a **{score}/100**\n",
                        value=f"{counter}")
        await ctx.send(embed=embed)

    @commands.command(brief="Roll a number between the given range", usage="[lower_bound] [upper_bound]")
    async def roll(self, ctx, num1: int = 1, num2: int = None):
        if not num2:
            num2 = 1
            if num1 == 1:
                num2 *= 100
        if num1 < 0:
            num1 = 0
        if num2 < 0:
            num2 = 0

        if num1 > max_int_size - 1:
            num1 = max_int_size - 1
        if num2 > max_int_size - 1:
            num2 = max_int_size - 1

        nums = (min(num1, num2), max(num1, num2))
        roll = random.randint(nums[0], nums[1])
        await ctx.send(f"**{ctx.author.display_name}** rolled {nums[0]}-{nums[1]} and got **{roll}**")

    # Helper function for rps command
    async def ask_rps(self, user, r_msg, field_index):
        emotes = {"\u270a": "rock", "\u270b": "paper", "\u270c": "scissors"}
        pm_embed = self.default_embed()
        result_embed = r_msg.embeds[0]
        pm_embed.title = result_embed.title
        pm_embed.description = f"[Link to challenge]({r_msg.jump_url})"
        pm_embed.add_field(name="**React to this message with your choice**",
                           value="\n".join([f"{key} **{emotes[key]}**" for key in emotes]))
        pm_embed.set_footer(text="Waiting for reaction...")
        try:
            msg = await user.send(embed=pm_embed)
        except d.Forbidden as e:
            result_embed.set_field_at(field_index, name=f"{user.display_name}", value="Error")
            await r_msg.edit(embed=result_embed)
            await r_msg.channel.send(
                f"Could not send DM to {user}. User might have disabled DMs from server members, or has blocked me.")
            raise e
        for key in emotes:
            await msg.add_reaction(key)

        def check(r, u):
            return u == user and r.message.guild is None

        reaction = None
        try:
            reaction, user = await self.client.wait_for('reaction_add', timeout=15.0, check=check)
            pm_embed.set_footer(text=f"You chose {reaction.emoji} {emotes[reaction.emoji]}")
            result_embed.set_field_at(field_index, name=f"{user.display_name}",
                                      value="Ready")
        except asyncio.TimeoutError:
            pm_embed.set_footer(text="Timeout. You took too long to choose")
            result_embed.set_field_at(field_index, name=f"{user.display_name}", value="Timeout")
            result_embed.set_field_at(2, name="Match result: ", value="Cancelled", inline=False)
        await msg.edit(embed=pm_embed)
        await r_msg.edit(embed=result_embed)
        if not reaction:
            return None
        else:
            return emotes[reaction.emoji], reaction.emoji

    @commands.command(usage="<member> | <rock|paper|scissors>")
    async def rps(self, ctx, choice: typing.Union[d.Member, str] = None):
        """
        Play rock paper scissors
        You can either play vs the bot by saying rock | paper | scissors
        Or you can play vs a member by tagging them. You both will get a dm and must answer within 15 seconds.
        This feauture won't work if either player has the bot blocked.
        """
        options = ["rock", "paper", "scissors"]
        results = ["\u270a **rock** smashes **scissors**",
                   "\u270b **paper** covers **rock**",
                   "\u270c **scissors** cuts **paper**"]
        outcome = ["It's a tie, no one wins.", "You win!", "You lose!"]

        if type(choice) == d.Member:
            if choice == ctx.author:
                await ctx.send("You can't challenge yourself")
                return
            if choice == self.client.user:  # TODO
                return
            embed = self.default_embed()
            embed.title = f"{ctx.author.display_name} vs {choice.display_name}"
            embed.add_field(name=f"{ctx.author.display_name}", value="Choosing")
            embed.add_field(name=f"{choice.display_name}", value="Choosing")
            embed.add_field(name="Match result: ", value="Pending...", inline=False)
            msg = await ctx.send(
                content=f"{choice.mention} you have been challenged to RPS by {ctx.author.mention}",
                embed=embed)
            try:
                choices = await asyncio.gather(
                    self.ask_rps(ctx.author, msg, 0),
                    self.ask_rps(choice, msg, 1)
                )
            except d.Forbidden:
                await msg.delete()
                return
            if any([not y for y in choices]):
                return
            p1_choice = options.index(choices[0][0])
            p2_choice = options.index(choices[1][0])
            msg = await msg.channel.fetch_message(msg.id)
            embed = msg.embeds[0]
            embed.set_field_at(0, name=f"{ctx.author.display_name}", value=f"{choices[0][1]} {choices[0][0]}")
            embed.set_field_at(1, name=f"{choice.display_name}", value=f"{choices[1][1]} {choices[1][0]}")
            o_1 = p1_choice - p2_choice
            o_2 = p2_choice - p1_choice
            if o_1 == -2 or o_1 == 1:
                embed.title = f"{ctx.author.display_name} wins!"
                embed.set_field_at(2, name=f"Match result: ", value=f"{results[p1_choice]}", inline=False)
                embed.set_thumbnail(url=ctx.author.avatar_url)
            elif o_2 == -2 or o_2 == 1:
                embed.title = f"{choice.display_name} wins!"
                embed.set_field_at(2, name=f"Match result: ", value=f"{results[p2_choice]}", inline=False)
                embed.set_thumbnail(url=choice.avatar_url)
            else:
                embed.set_field_at(2, name=f"Match result: ", value=outcome[0], inline=False)
            await msg.edit(embed=embed)
        else:
            if not choice or choice.lower() not in options:
                await ctx.send("Invalid choice: choose between rock, paper, or scissors\n"
                               f"Or challenge someone with `{ctx.prefix}rps @name`")
                return
            bot_choice = random.randint(0, 2)
            player_choice = options.index(choice)
            outcome_index = bot_choice - player_choice
            result = results[-3 + player_choice + (1 * (outcome[outcome_index] == outcome[2])) or -3]
            await ctx.send(outcome[outcome_index] + (f"\n{result}" * (outcome_index != 0)))

    @commands.command(brief="View server info")
    async def serverinfo(self, ctx):
        embed = self.default_embed()
        memcount = 0
        botcount = 0
        for m in ctx.guild.members:
            if m.bot:
                botcount += 1
            else:
                memcount += 1
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.title = f"{ctx.guild.name}"
        embed.description = f"**Nitro level:** {ctx.guild.premium_tier}\n" \
                            f"**ID:** {ctx.guild.id}"
        embed.add_field(name="Owner", value=ctx.guild.get_member(ctx.guild.owner_id), inline=False)
        embed.add_field(name="Region", value=str(ctx.guild.region))
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name="Created", value=ctx.guild.created_at.strftime("%d %b %Y, %H:%M"))
        embed.add_field(name="Members", value=str(memcount))
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name="Bots", value=str(botcount))
        await ctx.send(embed=embed)

    @commands.command(brief="Get info about a user", usage="[member]")
    async def userinfo(self, ctx, user: d.Member = None):
        if not user:
            user = ctx.author
        embed = self.default_embed()
        embed.set_thumbnail(url=user.avatar_url)
        embed.description = f"User ID: {ctx.author.id}"
        embed.add_field(name="Discord name", value=str(user))
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name="Nickname", value=str(user.nick))
        join_pos = sorted(ctx.guild.members, key=lambda m: m.joined_at).index(user) + 1
        ordinal = (lambda n: "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4]))(join_pos)
        embed.add_field(name="Join position", value=ordinal)
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name="Joined this server", value=user.joined_at.strftime("%d %b %Y, %H:%M"))
        embed.add_field(name="Account created", value=user.created_at.strftime("%d %b %Y, %H:%M"), inline=False)
        await ctx.send(embed=embed)

    @commands.command(usage="[user]", aliases=["av"])
    async def avatar(self, ctx, user: d.Member = None):
        """
        Shows a user's avatar
        """
        if not user:
            user = ctx.author
        ext = 'gif' if user.is_avatar_animated() else 'png'
        await ctx.send(file=d.File(BytesIO(await user.avatar_url.read()), f"{user.id}.{ext}"))

    @commands.command(usage="<member_one> [member_two]")
    async def ship(self, ctx, user1: d.Member, user2: d.Member = None):
        """
        Ships two members.
        If [member_two] is left out, then it will ship with the user who called the command.
        """
        p_bar = ["<:pb_l_e:726137285103583232>", "<:pb_l_f:726148987593162892>", "<:pb_m_e:726137284793204737>",
                 "<:pb_m_f:726147272139735080>", "<:pb_r_e:726137284453466183>", "<:pb_r_f:726149803540480120>"]
        if not user2:
            user2 = ctx.author

        first_half = user1.display_name[:round(len(user1.display_name) / 2)]
        second_half = user2.display_name[round(len(user2.display_name) / 2):]
        ship_name = first_half + second_half
        
        embed = self.default_embed()
        random.seed((user1.id * user2.id)/2)
        score = random.randint(0, 100)
        filled_progbar = round(score / 100 * 10)
        if filled_progbar <= 0:
            counter = p_bar[0] + 8 * p_bar[2] + p_bar[4]
        elif filled_progbar >= 10:
            counter = p_bar[1] + 8 * p_bar[3] + p_bar[5]
        else:
            counter = p_bar[1] + p_bar[3] * (filled_progbar - 1) + (p_bar[2] * (9 - filled_progbar)) + p_bar[4]

        embed.title = f"'{user1.display_name}' + '{user2.display_name}' = '{ship_name}' \u2764"
        embed.add_field(name=f"{score}% Love", value=counter)

        await ctx.send(embed=embed)

    @commands.command(brief="Roll the slot machine")
    async def slots(self, ctx):
        fruits = json.loads(self.config["REPLIES"]["fruits"])
        f1 = random.choice(fruits)
        f2 = random.choice(fruits)
        f3 = random.choice(fruits)
        out = f"**{ctx.author.display_name}** rolled the slots...\n" \
              f"**[{f1} {f2} {f3}]**\n"
        if f1 == f2 == f3:
            out += "and won! \uD83C\uDF89"
        elif f1 == f2 or f1 == f3 or f2 == f3:
            out += "and almost won (2/3)"
        else:
            out += "and lost..."
        await ctx.send(out)

    @commands.command(brief="Throw something at someone", usage="<member>")
    async def throw(self, ctx, user: d.Member = None):
        if not user:
            await ctx.send("Who are you throwing that at?")
            return
        quotes = json.loads(self.config["REPLIES"]["throw"])
        author_q = random.choice(quotes["targetQuotes"])
        target_q = random.choice(quotes["authorQuotes"])
        item = random.choice(quotes["items"])
        await ctx.send(f"**{ctx.author.display_name}** threw {item} at **{user.display_name}**\n\n"
                       f"**{user.display_name}**: {target_q}\n"
                       f"**{ctx.author.display_name}**: {author_q}")

    @commands.command(brief="Get urban dictionary definition for a word", usage="<search_term>")
    async def urban(self, ctx, *, search):
        def percent_encoding(string):
            result = ''
            accepted = [c for c in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~'.encode('utf-8')]
            for char in string.encode('utf-8'):
                result += chr(char) if char in accepted else '%{}'.format(hex(char)[2:]).upper()
            return result

        search = percent_encoding(search)
        try:
            async with self.session.get(self.config["API"]["urban"].format(search)) as r:
                res = await r.json()
                if r.status == 404:
                    await ctx.send(res["message"])
                    return
                embed = self.default_embed()
                embed.description = f"**{res['term']}**" \
                                    f"\n[Definition link]({res['url']})" \
                                    f"\nAuthor: [{res['author']}]({res['author_url']})"
                embed.add_field(name="Definition: ", value=res["definition"], inline=False)
                embed.add_field(name="Example: ", value=res["example"], inline=False)
                embed.add_field(name="Date posted:", value=res["posted"][:10])
                await ctx.send(embed=embed)
        except Exception:
            await ctx.send("I couldn't access the dictionary")

    async def fetch_trivia_data(self, ctx):
        elapsed = False
        if self.trivia_categs["checked"]:
            t = datetime.now() - self.trivia_categs["checked"]
            if t.seconds / 60 >= 30:
                elapsed = True
        if elapsed or not self.trivia_categs["checked"] or not self.trivia_categs["categs"]:
            try:
                async with ctx.channel.typing():
                    async with self.session.get(self.config["API"]["trivia_count"]) as r:
                        count = (await r.json())["categories"]
                    async with self.session.get(self.config["API"]["trivia_categories"]) as r:
                        categs = (await r.json())["trivia_categories"]

                def add_count(dic):
                    c = count[str(dic["id"])]["total_num_of_verified_questions"]
                    dic["name"] = re.sub(".*: ", "", dic["name"])
                    dic["count"] = c
                    return c

                categs = sorted(categs, key=lambda x: add_count(x), reverse=True)
                self.trivia_categs = {"checked": datetime.now(), "categs": categs}
                return categs
            except Exception:
                await ctx.send("Error connecting to trivia API")
        else:
            return self.trivia_categs["categs"]

    async def ask_trivia_question(self, ctx, answer, options=None, time_limit=15.0):
        have_answered = []

        def on_message(m):
            if not m.channel == ctx.channel:
                return False
            if m.author in have_answered:
                return False
            elif m.content.lower() == answer.lower():
                return True
            elif options:
                try:
                    opt = options[int(m.content) - 1]
                    have_answered.append(m.author)
                    if opt == answer:
                        return True
                except:
                    pass

            return False

        try:
            msg = await self.client.wait_for("message", check=on_message, timeout=time_limit)
            return msg.author
        except asyncio.TimeoutError:
            return None

    async def make_trivia(self, ctx, categ):
        start_embed = self.default_embed()
        # Get the category, and save it in categ
        categories = await self.fetch_trivia_data(ctx)
        if not categories:
            return
        if type(categ) == int:
            try:
                categ = categories[categ]
            except IndexError:
                raise commands.UserInputError
        else:
            for c in categories:
                if c["name"] == categ.title():
                    categ = c
                    break
            else:
                raise commands.UserInputError
        start_embed.title = categ["name"]
        start_embed.set_author(name=ctx.author.display_name, icon_url=ctx.author.avatar_url)
        instructions = f"There are {categ['count']} unique questions in this category!\n\n" \
                       "**Instructions**\n" \
                       "I will ask 10 questions, whoever gets the most correct wins!\n" \
                       "If a question has multiple options, you can type the number of the option " \
                       "***ONLY once***. However you can try as many times by typing out the answer.\n\n"
        start_embed.description = instructions + "Fetching questions <a:loading:729059787093835857>"

        start_msg = await ctx.send(embed=start_embed)

        # Get token if older than 10 minutes
        fetch_token = False
        if ctx.channel.id in self.trivia_tokens:
            if (datetime.now() - self.trivia_tokens[ctx.channel.id]["made"]).seconds / 60 >= 10:
                fetch_token = True
        else:
            fetch_token = True
        if fetch_token:
            try:
                async with self.session.get(self.config["API"]["trivia_token"]) as r:
                    token = (await r.json())["token"]
                    self.trivia_tokens[ctx.channel.id] = {"made": datetime.now(), "token": token}
            except Exception:
                await ctx.send("Error connecting to trivia API")
                self.trivia_tokens.pop(ctx.channel.id, "")
                await start_msg.delete()
                return
        else:
            token = self.trivia_tokens[ctx.channel.id]["token"]
        # Fetch 10 questions using token
        questions = None
        no_questions = 10
        attempts = 0
        async with ctx.channel.typing():
            while attempts < 3:  # Try 3 times to get the questions
                async with self.session.get(self.config["API"]["trivia_questions"].format(
                        no_questions,
                        str(categ["id"]),
                        str(token))) as r:
                    res = await r.json()
                if res["response_code"] == 4:
                    async with self.session.get(self.config["API"]["trivia_token_reset"].format(str(token))) as r:
                        self.trivia_tokens[ctx.channel.id]["made"] = datetime.now()
                elif res["response_code"] == 3:
                    self.trivia_tokens.pop(ctx.channel.id, "")
                    await start_msg.delete()
                    raise RuntimeError("Token does not exist, try again.")
                elif res["response_code"] == 0:
                    questions = res["results"]
                    break
                attempts += 1
                if attempts >= 3:
                    self.trivia_tokens.pop(ctx.channel.id, "")
                    await start_msg.delete()
                    raise RuntimeError("Couldn't retrieve questions, try again.")
        start_embed.description = instructions + "Fetching questions <:tick:729059775173754960>"
        await start_msg.edit(embed=start_embed)

        participants = {}
        # Ask the questions:
        try:
            diff = {"hard": ":red_circle: Hard", "medium": ":yellow_circle: Medium", "easy": ":green_circle: Easy"}
            for i, question in enumerate(questions):
                embed = self.default_embed()
                question_text = html.unescape(question['question'])
                question_answer = html.unescape(question["correct_answer"])
                options = []
                if question["type"] == "multiple":
                    if len(question_answer.split(" ")) >= 3 \
                            or "which" in question_text.lower() or "is not a" in question_text.lower():
                        options += [question["correct_answer"]] + question["incorrect_answers"]
                embed.description = f"({i+1}/{no_questions}) **{question_text}**\n\n"
                if options:
                    embed.description += "**Options**\n"
                    random.shuffle(options)
                    for j, opt in enumerate(options):
                        embed.description += f"[{j+1}]  {html.unescape(opt)}\n"
                embed.description += f"\n\n{diff[question['difficulty']]}\n\n"
                await ctx.send(embed=embed)
                async with ctx.channel.typing():
                    winner = await self.ask_trivia_question(ctx, question_answer, options)
                if not winner:
                    embed.description = "**Time out!**"
                    embed.add_field(name="Answer:", value=f"**{question_answer}**")
                    await ctx.send(embed=embed)
                else:
                    await ctx.send(f"Correct! {winner.mention} gets **+1**")
                    if winner in participants:
                        participants[winner] += 1
                    else:
                        participants[winner] = 1
                await asyncio.sleep(1)
        except asyncio.CancelledError:
            if participants:
                await ctx.send("Stopped trivia")
                return participants
            else:
                raise
        return participants

    @commands.group(invoke_without_command=True, case_insensitive=True, usage="<category_name> | <category_id>")
    @commands.max_concurrency(1, commands.BucketType.channel)
    async def trivia(self, ctx, *, category: typing.Union[int, str]):
        """
            Start a game of trivia!
            10 questions are asked, winner is whoever answers the most correct.
            If a question is multiple choice, you can answer by either typing 1-4 or writing out the answer.
            If you answer with a number, you can't answer again, but you can try as many times if you type an answer.
            This is to prevent spamming numbers, but not punish typos.
        """
        task = self.client.loop.create_task(self.make_trivia(ctx, category))
        self.on_going_trivia[ctx.channel.id] = {"task": task, "author": ctx.author}
        try:
            players = await task
            if not players:
                await ctx.send("No one answered anything correctly. How dissapointing.")
            else:
                players = sorted(players.items(), key=lambda x: x[1], reverse=True)
                embed = self.default_embed()
                for i, p in enumerate(players):
                    user, points = p
                    if i == 0:
                        embed.set_thumbnail(url=user.avatar_url)
                        embed.description = f":ribbon: **{user.display_name}**  -  {points}\n\n"
                    else:
                        embed.description += f"{i+1}. {user.display_name}  -  {points}\n"
                await ctx.send(embed=embed)
        except asyncio.CancelledError:
            await ctx.send("Stopped trivia")
        self.on_going_trivia.pop(ctx.channel.id, "")

    @commands.command(usage="[comic_number]")
    async def xkcd(self, ctx, num: int = None):
        """
        Get an xkcd comic!
        If no comic number is given, a random comic will be displayed instead.
        """
        embed = self.default_embed()
        try:
            async with self.session.get(self.config["API"]["xkcd"].format("")) as r:
                comic_num = await r.json()
                comic_num = int(comic_num["num"])
            if num and 1 <= num <= comic_num:
                comic_num = num
            else:
                comic_num = random.randint(1, comic_num)
            async with self.session.get(self.config["API"]["xkcd"].format(str(comic_num))) as r:
                res = await r.json()
        except Exception:
            await ctx.send("I couldn't connect to xkcd's api.")
            return
        embed.description = f"**{res['title']}**\n" \
                            f"[xkcd {comic_num}](https://xkcd.com/{comic_num}/)\n" \
                            f"*{res['alt']}*"
        embed.set_image(url=res["img"])
        await ctx.send(embed=embed)

    @trivia.command(name="list")
    async def trivia_list(self, ctx):
        """
        Lists all the available trivia categories.
        """
        embed = self.default_embed()
        categories = await self.fetch_trivia_data(ctx)
        if not categories:
            return

        embed.description = f"**Trivia Categories**\nStart one with {ctx.prefix}trivia <category_name | category_id>"
        out = "```\n"
        for i, categ in enumerate(categories):
            if categ["count"] >= 45:
                out += f"{i}\t{categ['name']}\n".expandtabs(4)
        out += "\n```"
        embed.add_field(name="ID\t - \tCategory Name".expandtabs(2), value=out)
        await ctx.send(embed=embed)

    @trivia.command()
    async def stop(self, ctx):
        """
        Stops an ongoing trivia.
        Trivia can only be stopped by someone with administrator rights, or by the person who started the trivia.
        """
        if ctx.channel.id not in self.on_going_trivia:
            await ctx.send("No ongoing trivia in this channel")
            return
        else:
            trivia = self.on_going_trivia[ctx.channel.id]
            if ctx.author.guild_permissions.administrator or trivia["author"] == ctx.author:
                trivia["task"].cancel()
            else:
                await ctx.send("Sorry, only the person who started the trivia, or an administrator can cancel it.")

    @trivia.error
    async def on_command_error(self, ctx, error):
        embed = self.default_embed()
        if isinstance(error, commands.MaxConcurrencyReached):
            embed.add_field(name="Can't start trivia",
                            value="There is already an ongoing trivia session in this channel\n"
                                  f"To cancel it type `{ctx.prefix}trivia stop`")
            await ctx.send(embed=embed)
        else:
            delattr(ctx.command, "on_error")


# Load Cog

def setup(client):
    fun_cog = Fun(client)
    client.add_cog(fun_cog)
