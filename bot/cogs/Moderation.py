import json
import typing
import asyncio
import logging
import discord as d
from io import BytesIO
from discord.ext import tasks, commands
from datetime import datetime, timedelta
from PIL import Image


class Moderation(commands.Cog):
    """
    Chat ban a user, or set up automatically banned words.
    """

    def __init__(self, client):
        self.client = client
        self.color = 0xfef200
        # Logs
        self.logger = logging.getLogger("jail_cog")
        self.logger.setLevel(logging.DEBUG)
        self.wordlist = {}
        handler = logging.FileHandler(filename='jail.log', encoding='utf-8', mode='a')
        handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
        self.logger.addHandler(handler)
        self.logger.debug("Started")

        self.jail_loop.start()

    def cog_unload(self):
        self.jail_loop.cancel()

    def default_embed(self, thumbnail=False):
        embed = d.Embed(color=self.color)
        embed.set_footer(text="For a full set of commands type help jail")
        if thumbnail:
            embed.set_thumbnail(url=self.client.user.avatar_url)
        return embed

    @tasks.loop(seconds=1.0)
    async def jail_loop(self):
        async with self.client.pool.acquire() as con:
            jail_list = await con.fetch("SELECT server_id, member_id, release_at FROM jail "
                                        "WHERE release_at IS NOT NULL")
            if not jail_list:
                self.jail_loop.stop()
            for record in jail_list:
                try:
                    if record["release_at"] <= datetime.now():
                        server = self.client.get_guild(record["server_id"])
                        if not server:
                            async with con.transaction():
                                await con.execute("DELETE FROM servers WHERE server_id = $1", record["server_id"])
                            continue
                        member = server.get_member(record["member_id"])
                        if member and server:
                            await self.free_from_jail(self.client.user, member, server)
                        else:
                            async with con.transaction():
                                await con.execute("DELETE FROM jail WHERE server_id = $1 AND member_id = $2",
                                                  record["server_id"], record["member_id"])
                except Exception:
                    continue

    @jail_loop.before_loop
    async def jail_loop_before(self):
        await self.client.wait_until_ready()

    async def check_if_setup(self, ctx):
        async with self.client.pool.acquire() as con:
            j_roles = await con.fetchval("SELECT jail_roles FROM jail_settings WHERE server_id = $1", ctx.guild.id)
            if j_roles:
                change = False
                for i, pair in enumerate(j_roles):
                    m, j = pair
                    m = ctx.guild.get_role(m)
                    j = ctx.guild.get_role(j)
                    if not m or not j:
                        change = True
                        j_roles.pop(i)
                if change:
                    async with con.transaction():
                        await con.execute("UPDATE jail_settings SET jail_roles = $1 "
                                          "WHERE server_id = $2", j_roles, ctx.guild.id)

        if not j_roles:
            embed = self.default_embed()
            embed.title = "No jail roles set"
            embed.description = "Configure the bot with the following commands:"
            embed.add_field(name=f"{ctx.prefix}jail addroles @Jail Role @Member Role",
                            value="Members with the @Member role will get the @Jail role when jailed", inline=True)
            embed.add_field(name=f"{ctx.prefix}jail removerole @Member",
                            value="Removes Member role from list of jail roles",
                            inline=True)
            embed.add_field(name=f"{ctx.prefix}jail listroles",
                            value="Shows a list of Member-Jail roles in order of priority",
                            inline=True)
            return embed
        return None

    async def allowed_to_jail(self, ctx, target=None):
        if ctx.author.guild_permissions.administrator:
            return True
        #if target and target == ctx.author and ctx.guild.id == 191261256034811904:
         #   return True

        async with self.client.pool.acquire() as con:
            server_perms = await con.fetch("SELECT snowflake_id, jail, signup FROM permissions "
                                           "WHERE server_id = $1", ctx.guild.id)

        allowed = False
        for record in server_perms:
            if record["snowflake_id"] == ctx.author.id:
                if record["jail"]:
                    allowed = True
                elif record["jail"] is not None:
                    return False
            else:
                role = ctx.guild.get_role(record["snowflake_id"])
                if role:
                    if role in ctx.author.roles:
                        if record["jail"]:
                            allowed = True
                        elif record["jail"] is not None:
                            return False
        return allowed

    async def is_in_jail(self, user: d.Member, server: d.Guild, check_in_db=False):

        async with self.client.pool.acquire() as con:
            jail_roles = await con.fetchval("SELECT jail_roles FROM jail_settings WHERE server_id = $1", server.id)
            is_in_db = await con.fetchval("SELECT member_id FROM jail "
                                          "WHERE member_id = $1 AND server_id = $2", user.id, server.id) is not None
            jail_roles = [server.get_role(x[1]) for x in jail_roles]
            has_jail_role = False
            for role in jail_roles:
                if role in user.roles:
                    has_jail_role = True
                    break
            if not has_jail_role and is_in_db:
                async with con.transaction():
                    await con.execute("DELETE FROM jail WHERE member_id = $1 AND server_id = $2", user.id, server.id)
        if not check_in_db:
            return has_jail_role
        else:
            return has_jail_role, is_in_db

    async def send_to_jail(self, jailer: d.Member, user: d.Member, server: d.Guild, times: tuple):
        embed = self.default_embed()
        append = False  # TODO, instead of replacing just append jail role
        new_roles = []
        update = False
        async with self.client.pool.acquire() as con:
            jail_roles = await con.fetchval("SELECT jail_roles FROM jail_settings WHERE server_id = $1", server.id)
            prev_roles = await con.fetchval("SELECT role_list FROM jail "
                                            "WHERE server_id = $1 AND member_id = $2", server.id, user.id)

            for mem_role, jail_role in jail_roles:
                mem_role = server.get_role(mem_role)
                if mem_role in user.roles or prev_roles:
                    if append:
                        new_roles += prev_roles
                    if user in server.premium_subscribers:
                        new_roles = [role for role in server.roles if role.managed and role in user.roles]

                    jail_role = server.get_role(jail_role)
                    if not jail_role:
                        embed.add_field(name="Error", value="No roles configured")
                        return embed, None
                    new_roles.append(jail_role)
                    tr = con.transaction()
                    await tr.start()
                    if not prev_roles:
                        embed_name = f"{user.display_name} was sent to jail."
                        prev_roles = [role.id for role in user.roles[1:]]
                        await con.execute("INSERT INTO jail VALUES ($1,$2,$3,$4,$5,$6)",
                                          server.id, user.id, jailer.id, prev_roles, times[0],
                                          times[1])
                    else:
                        update = True
                        embed_name = f"Updated jail time for {user.display_name}"
                        await con.execute("UPDATE jail SET release_at=$1, jailed_by=$2 "
                                          "WHERE member_id=$3 AND server_id=$4", times[1], jailer.id, user.id,
                                          server.id)

                    if user == jailer:
                        embed_name = f"{user.display_name} jailed themselves <:pepega:597420506563739650>"

                    reason = f"{user} was jailed by {jailer}"
                    if not update:
                        try:
                            await user.edit(roles=new_roles, reason=reason)
                        except d.errors.Forbidden:
                            await tr.rollback()
                            embed.add_field(name="Insufficient permissions", value="Make sure my role has priority")
                            return embed, None
                    await tr.commit()
                    embed_val = f"See `jail info @{user.display_name}` for more."
                    if times[1]:
                        # Task is running
                        if self.jail_loop.get_task() is None or self.jail_loop.get_task().done():
                            self.jail_loop.start()
                        embed_val += f"\nTime remaining: {format_time(times[1] - times[0])}"
                        reason += f" for {format_time(times[1] - times[0])}"
                    self.logger.info(f"[{server.name}] {reason}")
                    embed.add_field(name=embed_name, value=embed_val, inline=False)
                    try:
                        pic = await self.make_jail_pic(user)
                    except:
                        pic = None
                    if pic:
                        embed.set_thumbnail(url="attachment://" + pic.filename)
                    return embed, pic

            embed.add_field(name="User has no member role.", value=f"See jail listroles for more info.",
                            inline=False)
            return embed, None

    async def free_from_jail(self, jailer: d.Member, user: d.Member, server: d.Guild):
        message = ""
        new_roles = []

        async with self.client.pool.acquire() as con:
            jail_row = await con.fetchrow("SELECT release_at, role_list FROM jail "
                                          "WHERE server_id = $1 AND member_id = $2", server.id, user.id)

            jail_roles = await con.fetchval("SELECT jail_roles FROM jail_settings WHERE server_id = $1", server.id)
            release_time = None
            if not jail_row:
                message += f"{user.display_name} has no previous roles saved to restore."
                for mem_role, jail_role in jail_roles:
                    jail_role = server.get_role(jail_role)
                    if jail_role in user.roles:
                        new_roles.append(mem_role)
                        message += " Adding default role.\n"
                        break
                else:  # If loop didn't break
                    message += f"\nCouldn't restore {user.display_name}'s roles."
            else:
                new_roles = jail_row["role_list"]
                release_time = jail_row["release_at"]

            r_str = ""
            if release_time:
                td = release_time - datetime.now()
                if rm_microsecs(td) > timedelta(0):
                    r_str += format_time(td) + " early."
            reason = f"{user} was freed by {jailer} " + r_str
            tr = con.transaction()
            await tr.start()
            try:
                new_roles = [server.get_role(x) for x in new_roles]
                await user.edit(roles=new_roles, reason=reason)
                self.logger.info(f"[{server.name}] {reason}")
                await con.execute("DELETE from jail WHERE member_id = $1 AND server_id = $2", user.id, server.id)
                message += f"<@{user.id}> has been freed " + r_str
            except d.Forbidden:
                message = "I do not have permission to do that."
                await tr.rollback()
            else:
                await tr.commit()

        if not message:
            message = f"Couldn't restore {user.display_name}'s roles."
        return message

    async def send_jail_pm(self, server: d.Guild, user: d.Member):
        async with self.client.pool.acquire() as con:
            msg = await con.fetchval("SELECT jail_message FROM jail_settings "
                                     "WHERE server_id = $1", server.id)
            jail_row = await con.fetchrow("SELECT jailed_by, release_at FROM jail "
                                          "WHERE server_id = $1 AND member_id = $2", server.id, user.id)
            jailer = server.get_member(jail_row["jailed_by"])
            r_at = jail_row["release_at"]
        if not msg:
            return

        time_left = format_time(r_at - datetime.now()) if r_at else "Indefinite"
        jailer = jailer.display_name if jailer else "Unavailable"

        dic = {"{server}": server.name, "{time_left}": time_left, "{jailer}": jailer, "{user}": user.display_name}
        msg = replace(msg, dic)
        await user.send(msg)

    @commands.guild_only()
    @commands.group(invoke_without_command=True, case_insensitive=True, usage="<member> [time]")
    async def jail(self, ctx, user: d.Member, *args):
        """
        Sends someone to jail.
        Removes all the member's roles and adds the appropriate jail role.
        Can only remove roles below the bot's own role.
        If [time] is specified then bot will automatically free the user, else the free command must be used.
        Format for [time] is "Zd Yh Xm", eg: "30m", "3h 15m"
        See `{prefix}help jail addrole` for more info regarding member-jail role pairs.
        """
        missing_setup = await self.check_if_setup(ctx)
        if missing_setup:
            await ctx.send(embed=missing_setup)
            return
        if not await self.allowed_to_jail(ctx, user):
            raise commands.CheckFailure
        embed = self.default_embed()
        if ctx.invoked_with.lower() == "jail":
            has_rt = False
            parsed_time = (datetime.now(), None)
            if user == self.client.user:
                await ctx.send("Don't be cheeky <:duckstab:595643456269975573>")
                return
            try:
                parsed_time = parse_time(args)
                if parsed_time[1]:
                    has_rt = True
            except Exception:
                pass
            if args and not has_rt:
                embed.add_field(name="Couldn't parse time",
                                value=f"Try something like:\n`{ctx.prefix}jail @Name 1d 4h 8m`"
                                      f"\n`{ctx.prefix}jail @Name 8m`",
                                inline=False)
                await ctx.send(embed=embed)
                return
            is_in_jail = await self.is_in_jail(user, ctx.guild)
            if is_in_jail and user == ctx.author:
                return
            if not is_in_jail or has_rt:
                embed, file = await self.send_to_jail(ctx.author, user, ctx.guild, parsed_time)
                await ctx.send(embed=embed, file=file)
                try:
                    if ctx.message.author == user:
                        await user.send(
                            f"**Psst you can specify a time with {ctx.prefix}jail Xminutes Yhours Ydays** "
                            "<:duckbot:652787079645888514>")
                    elif file and not is_in_jail:
                        await self.send_jail_pm(ctx.guild, user)
                except d.Forbidden:
                    pass
            else:
                embed.add_field(name="User is already in jail",
                                value=f"See `{ctx.prefix}jail info @{user.display_name}` for more.\n"
                                      f"\nIf you want to update the release time, try something like:"
                                      f"\n`{ctx.prefix}jail @Name 1d 4h 8m`\n`{ctx.prefix}jail @Name 8m`")
                await ctx.send(embed=embed)

    @jail.command(usage="<member>")
    @commands.guild_only()
    async def info(self, ctx, user: d.Member):
        """
        Shows info about a member's jail status
        Shows the time done, and time remaining (if any) until they are automatically released.
        If the person calling this can use jail, then it will show who jailed <member> and their previous roles.
        """
        embed = self.default_embed()
        embed.set_author(name=user.display_name)
        has_jail_role, is_in_db = await self.is_in_jail(user, ctx.guild, check_in_db=True)
        is_jailer = await self.allowed_to_jail(ctx)

        async with self.client.pool.acquire() as con:
            jail_row = await con.fetchrow("SELECT * FROM jail "
                                          "WHERE server_id = $1 AND member_id = $2", ctx.guild.id, user.id)
        f = None
        if is_in_db or has_jail_role:
            if jail_row:
                j_at = jail_row["jailed_at"]
                r_at = jail_row["release_at"]
                jailer = jail_row["jailed_by"]
                prev_roles = jail_row["role_list"]
            else:
                j_at = r_at = jailer = prev_roles = None
            if is_jailer:
                if prev_roles:
                    prev_roles = [str(ctx.guild.get_role(x)) or "<deleted role>" for x in prev_roles]
                else:
                    prev_roles = ["Unavailable"]
                if jailer:
                    jailer = f"<@{jailer}>"
                else:
                    jailer = "Unavailable"
            else:
                embed.description = "Must have jailer permissions to see <Hidden>"
                prev_roles = ["<Hidden>"]
                jailer = "<Hidden>"
            try:
                f = await self.make_jail_pic(user)
            except Exception:
                pass
            t_release = "Indefinite" if r_at is None else format_time(r_at - datetime.now())
            t_done = "Unavailable" if j_at is None else format_time(datetime.now() - j_at)
            embed.add_field(name="Time remaining:", value=t_release, inline=True)
            embed.add_field(name="\u200b", value="\u200b", inline=True)
            embed.add_field(name="Time done:", value=t_done, inline=True)
            embed.add_field(name="Jailed by:", value=jailer, inline=False)
            embed.add_field(name="Previous roles:", value=", ".join(prev_roles) or "None")
            if f:
                embed.set_thumbnail(url="attachment://" + f.filename)
            if not is_in_db:
                embed.add_field(name="No information on this user found",
                                value="User is not in bot database, but currently has a jail role. "
                                      "Possible reason may be the user was manually jailed.", inline=False)
            if not has_jail_role:
                embed.add_field(name="User has no Jail role",
                                value="User was in bot database, but currently has no jail role. Possible "
                                      "reason may be the user was freed manually.", inline=False)
        else:
            embed.description = "Not in jail"
            embed.set_thumbnail(url=user.avatar_url)

        await ctx.send(file=f, embed=embed)

    @jail.command(name="list")
    @commands.guild_only()
    async def list_mems(self, ctx):
        """
        Show a list of all currently jailed users.
        """
        async with self.client.pool.acquire() as con:
            mem_list = await con.fetch("SELECT member_id FROM jail "
                                       "WHERE server_id = $1 ORDER BY jailed_at ASC", ctx.guild.id)
            jail_roles = await con.fetchval("SELECT jail_roles FROM jail_settings "
                                            "WHERE server_id = $1", ctx.guild.id)

        remove = []
        mem_list = list(filter(lambda x: x != None, [ctx.guild.get_member(x[0]) or remove.append(x[0]) for x in mem_list]))
        async with self.client.pool.acquire() as con:
            for r in remove:
                async with con.transaction():
                    await con.execute("DELETE FROM jail WHERE server_id = $1 AND member_id = $2", ctx.guild.id, r)
        jail_roles = [ctx.guild.get_role(x[1]) for x in jail_roles]

        embed = self.default_embed(thumbnail=True)
        embed.title = f"{ctx.prefix}jail info @User to view more\n"
        embed.set_author(name="Jail")
        out = ""
        for mem in mem_list:
            out += f"\n\u2022 {mem.display_name}"
        embed.description = out
        not_in_db = []
        for mem in ctx.guild.members:
            if len(mem.roles) == 2:
                if mem.roles[1] in jail_roles and mem not in mem_list:
                    not_in_db.append(mem)
        if not_in_db:
            out = ""
            for mem in not_in_db:
                out += f"\n\u2022 {mem.display_name}"
            embed.add_field(name="Jailed, but not in bot db: ", value=out, inline=False)
        else:
            if not out:
                embed.description = "Jail is empty."
        await ctx.send(embed=embed)

    @jail.command(usage="<member|role>")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def allow(self, ctx, arg: typing.Union[d.Role, d.Member]):
        """
        Gives permissions to use jail.
        """
        func = self.client.get_command("allow")
        await func(ctx, arg)

    @jail.command(usage="<member|role>")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def deny(self, ctx, arg: typing.Union[d.Role, d.Member]):
        """
        Removes permissions to use jail.
        This does not work as a blacklist, it will only remove existing permissions.
        """
        func = self.client.get_command("deny")
        await func(ctx, arg)

        # List what roles / who has permission to jail

    @jail.command()
    @commands.guild_only()
    async def perms(self, ctx):
        """
        Member/roles who can jail.
        This does not include bot-wide permissions.
        """
        func = self.client.get_command("perms")
        await func(ctx)

    @jail.command(usage="<jail_role> [member_role]", aliases=["addroles"])
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def addrole(self, ctx, jail_role: d.Role, member_role: d.Role = None):
        """
        Adds a <member> <jail> role pair.
        Meaning that someone with the <member> role will get <jail_role> when jailed.
        If [member_role] is not specified, @everyone will get the <jail_role>

        The order this command is called in is important.
        e.g If the command is called 3 times in this order:
            `jail addrole member-jail member`
            `jail addrole guest-jail friend`
            `jail addrole default-jail`
            Someone with just the member role will get the "member-jail" role.
            If they have both the member and friend roles will also get the "member-jail" role.
            Someone with just the friend role will get the guest-jail role.
            Someone with neither the member, or the friend role will get the "default-jail" role.
        """

        if not member_role:
            member_role = ctx.guild.default_role
        async with self.client.pool.acquire() as con:
            roles = await con.fetchval("SELECT jail_roles FROM jail_settings "
                                       "WHERE server_id = $1", ctx.guild.id)

            if roles:
                for mem_role, _ in roles:
                    if mem_role == member_role.id:
                        await ctx.send("That role has already been assigned")
                        return
                roles.append([member_role.id, jail_role.id])
            else:
                roles = [[member_role.id, jail_role.id]]
            async with con.transaction():
                await con.execute("""
                                   INSERT INTO jail_settings(server_id, jail_roles) 
                                      VALUES ($1, $2)
                                   ON CONFLICT (server_id) 
                                      DO UPDATE
                                      SET jail_roles = $2
                                   """, ctx.guild.id, roles)
            await ctx.send(f'`{member_role}` will now get {jail_role.name} role when jailed')

    @jail.command(usage="[member]")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def removerole(self, ctx, member_role: d.Role = None):
        """
        Removes a <member> <jail> role pair.
        If member is not specified it will remove the @everyone <jail> pair.
        """
        if not member_role:
            member_role = ctx.guild.default_role

        async with self.client.pool.acquire() as con:
            roles = await con.fetchval("SELECT jail_roles FROM jail_settings "
                                       "WHERE server_id = $1", ctx.guild.id)
            for i, pair in enumerate(roles):
                mem_role = pair[0]
                if mem_role == member_role.id:
                    roles.pop(i)
                    async with con.transaction():
                        await con.execute("UPDATE jail_settings SET jail_roles = $1 "
                                          "WHERE server_id = $2", roles, ctx.guild.id)
                        await ctx.send(f'`{member_role}` no longer has a jail role assigned to it')
                        return
            else:
                await ctx.send(f"Role `{member_role}` does not have a jail role associated to it.\n"
                               f"Check `{ctx.prefix}jail listroles` for more.")
                return

    @jail.command()
    @commands.guild_only()
    async def listroles(self, ctx):
        """
        Shows the <member> <jail> role pairs.
        This is in order, meaning that the bot will try to match pairs higher up in the list, before going down.
        """
        missing_setup = await self.check_if_setup(ctx)
        if missing_setup:
            await ctx.send(embed=missing_setup)
            return
        async with self.client.pool.acquire() as con:
            roles = await con.fetchval("SELECT jail_roles FROM jail_settings "
                                       "WHERE server_id = $1", ctx.guild.id)
        desc = "\n\n"
        for memrole, jrole in roles:
            desc += str(ctx.guild.get_role(memrole)) + " → " + str(ctx.guild.get_role(jrole)) + "\n"
        embed = self.default_embed()
        embed.title = "A user will get one jail role corresponding to their member role. In order of priority:"
        embed.description = desc
        embed.set_author(name="Member → Jail role")
        await ctx.send(embed=embed)

    @jail.command()
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def editpm(self, ctx):
        """
        Edit or remove jail pm
        """
        async with self.client.pool.acquire() as con:
            j_msg = await con.fetchval("SELECT jail_message FROM jail_settings WHERE server_id = $1", ctx.guild.id)
            embed = self.default_embed()
            embed.set_author(name=self.client.user.name, icon_url=self.client.user.avatar_url)
            embed.description = 'The next message you send will be set as the PM message users get when jailed.\n' \
                                'To cancel type "Cancel". To disable PM on jail type "Delete".'
            embed.add_field(name="Variables:",
                            value='```{server} = name of current server\n'
                                  '{time_left} = time until user is unjailed automatically.\n'
                                  '{jailer} = nickname of the person that jailed the user\n'
                                  '{user} = nickname of jailed user ```', inline=False)
            embed.add_field(name="Current jail pm:",
                            value=f"```{j_msg}```")
            await ctx.send(embed=embed)
            try:
                rply = await self.client.wait_for('message', check=lambda x: x.author == ctx.message.author,
                                                  timeout=300)
                msg = rply.content
                if msg.lower() == "cancel":
                    await ctx.send("Cancelled")
                    return
                if msg.lower() == "delete":
                    msg = None
                    await ctx.send("No PM will be sent on jail")
            except asyncio.TimeoutError:
                await ctx.send("Cancelled.")
                return

            async with con.transaction():
                await con.execute("UPDATE jail_settings SET jail_message = $1 WHERE server_id = $2", msg, ctx.guild.id)
            if msg:
                await ctx.send("Jail pm set to: \n" + msg)
                await rply.delete()

    @jail.command(usage="<word> <time>")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def banword(self, ctx, word, *args):
        """
        Adds a word to the ban list.
        Users who say this word will get automatically jailed for X amount of time, or indefinitely.
        Format for [time] is "Zd Yh Xm", eg: "30m", "3h 15m"
        """
        missing_setup = await self.check_if_setup(ctx)
        if missing_setup:
            await ctx.send(embed=missing_setup)
            return

        word = word.lower()
        embed = self.default_embed(thumbnail=True)
        failed = False
        try:
            curr_t, release_t = parse_time(args)
            if not release_t:
                failed = True
        except Exception:
            failed = True

        if failed:
            raise commands.UserInputError

        async with self.client.pool.acquire() as con:
            word_dict = await con.fetchval("SELECT banned_words FROM jail_settings WHERE server_id = $1", ctx.guild.id)
            if not word_dict:
                word_dict = {word: args}
            else:
                word_dict = json.loads(word_dict)
                word_dict[word] = args

            async with con.transaction():
                await con.execute("UPDATE jail_settings SET banned_words = $1 "
                                  "WHERE server_id = $2", json.dumps(word_dict), ctx.guild.id)
                self.wordlist[ctx.guild.id] = word_dict
        embed.add_field(name=f"Added '{word}' to the list of banned words.",
                        value=f"Saying this will result in a {format_time(release_t - curr_t)} jail."
                              f"\nTo undo type `{ctx.prefix}jail unbanword '{word}'`"
                              f"\nTo see all banned words type `{ctx.prefix}jail bannedwords`")

        await ctx.send(embed=embed)

    @jail.command(usage="<word>")
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def unbanword(self, ctx, word):
        """
        Remove a word from the ban list.
        """
        word = word.lower()
        embed = self.default_embed(thumbnail=True)

        async with self.client.pool.acquire() as con:
            word_dict = await con.fetchval("SELECT banned_words FROM jail_settings WHERE server_id = $1", ctx.guild.id)
            if word_dict:
                word_dict = json.loads(word_dict)
                if word_dict:
                    if not word_dict.pop(word, ""):
                        embed.add_field(name=f"'{word}' was not in the list of banned words.",
                                        value=f"To see the list of banned words use `{ctx.prefix}jail bannedwords`")
                    else:
                        async with con.transaction():
                            await con.execute("UPDATE jail_settings SET banned_words = $1 "
                                              "WHERE server_id = $2", json.dumps(word_dict), ctx.guild.id)
                            self.wordlist[ctx.guild.id] = word_dict
                        embed.add_field(name=f"Removed '{word}' from the list of banned words.",
                                        value=f"Users will no longer get jailed for saying '{word}'")
            else:
                embed.add_field(name="**No banned words configured.**",
                                value=f"To ban a word type: \n`{ctx.prefix}jail banword 'word' Xm Yh Zd`")
        await ctx.send(embed=embed)

    @jail.command()
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def bannedwords(self, ctx):
        """
        List of banned words in this server.
        Saying any of these words will make the bot jail the user.
        """
        embed = self.default_embed(thumbnail=True)
        async with self.client.pool.acquire() as con:
            word_dict = await con.fetchval("SELECT banned_words FROM jail_settings WHERE server_id = $1", ctx.guild.id)

        word_dict = json.loads(word_dict)
        if word_dict:
            out = [f"\u2022 '{key}' - {' '.join(word_dict[key])}" for key in word_dict.keys()]
            embed.add_field(name="**Banned words:**",
                            value="```\n" + "\n".join(out) + "\n```")
        else:
            embed.add_field(name="**No banned words configured.**",
                            value=f"To ban a word type: \n`{ctx.prefix}jail banword 'word' Xm Yh Zd`")
        self.wordlist[ctx.guild.id] = word_dict
        await ctx.send(embed=embed)

    @commands.guild_only()
    @commands.command(aliases=["unjail"], usage="<member>")
    async def free(self, ctx, user: d.Member):
        """
        Release someone from jail.
        Removes the jail role from <member> and restores their previous roles.
        Can only restore roles of users that were jailed with `jail` command, else the appropriate member role is used.
        All roles being added must be below this bot's role.
        """
        if ctx.author == user:
            if not ctx.author.guild_permissions.administrator and not await self.client.is_owner(ctx.author):
                await ctx.send("Sorry you brought this upon yourself")
                return
        if not await self.allowed_to_jail(ctx, user):
            raise commands.CheckFailure
        if await self.is_in_jail(user, ctx.guild):
            msg = await self.free_from_jail(ctx.author, user, ctx.guild)
            await ctx.send(msg)
        else:
            await ctx.send(f"{user.display_name} is not in jail.")

    @jail.error
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CheckFailure):
            embed = self.default_embed()
            embed.title = "Insufficient permissions"
            embed.description = "You need to have administrator rights or a jailer role."
            await ctx.send(embed=embed)
        else:
            delattr(ctx.command, "on_error")

    @commands.Cog.listener()
    async def on_message(self, message):
        server = message.guild
        if not server:
            return
        author = server.get_member(message.author.id)
        if not author or author == self.client.user:
            return
        if author.guild_permissions.administrator or author.top_role >= server.me.top_role:
            return
        if server.id not in self.wordlist:
            async with self.client.pool.acquire() as con:
                word_dict = await con.fetchval("SELECT banned_words FROM jail_settings WHERE server_id = $1", server.id)
                if word_dict:
                    word_dict = json.loads(word_dict)
                self.wordlist[server.id] = word_dict
        else:
            word_dict = self.wordlist[server.id]
        if not word_dict:
            return
        jail_time = []

        for key in word_dict:
            msg = message.content.lower()
            if len(key.split(" ")) == 1:
                msg = msg.split(" ")
            if key in msg:
                if await self.is_in_jail(author, server):
                    return
                jail_time.append(parse_time(word_dict[key]))

        if jail_time:
            jail_time = min(jail_time)
            embed, file = await self.send_to_jail(server.me, author, server, jail_time)
            if not file:
                return
            await message.channel.send(embed=embed, file=file)
            try:
                await self.send_jail_pm(server, author)
            except d.Forbidden:
                pass

    async def make_jail_pic(self, user: d.Member):
        def overlay(avatar_bytes):
            with Image.open(BytesIO(avatar_bytes)) as avatar:
                with Image.open(self.client.dirpath / "resources/images/bars.png") as bars:
                    bars = bars.convert("RGBA")
                    avatar.thumbnail([128, 128])
                    avatar.paste(bars, (0, 0), bars)
                    buffer = BytesIO()
                    avatar.save(buffer, "png")
            buffer.seek(0)
            return buffer

        avatar_b = await user.avatar_url_as(format="png").read()
        output_buffer = await self.client.loop.run_in_executor(None, lambda: overlay(avatar_b))
        return d.File(fp=output_buffer, filename=f"{user.id}-jail_pic.png")


def parse_time(args):
    t = {"d": 0, "h": 0, "m": 0}
    parsed = False
    for arg in args:
        arg = arg.lower()
        for k in t.keys():
            if k in arg:
                parsed = True
                t[k] += abs(int(arg.split(k)[0]))
                break
    curr_time = datetime.now()
    release_time = None if not parsed else curr_time + timedelta(days=t["d"], hours=t["h"], minutes=t["m"])
    if release_time and release_time - curr_time == timedelta(0):
        release_time = None
    return curr_time, release_time


def rm_microsecs(time_delta):
    return time_delta - timedelta(microseconds=time_delta.microseconds)


def format_time(td):
    t = {"day": td.days, "hour": td.seconds // 3600, "minute": (td.seconds // 60) % 60,
         "second": td.seconds % 60}
    out = "".join(
        list(filter(None, map(lambda k: (str(t[k]) + " " + k + "s" * (t[k] > 1) + ", ") * (t[k] > 0), t.keys())))[:2])
    return (out[:-2] if td > timedelta(0) else "None") or "None"


def replace(string, dic):
    for x, y in dic.items():
        string = string.replace(x, y)
    return string


# Load Cog

def setup(client):
    jail_cog = Moderation(client)
    client.add_cog(jail_cog)
