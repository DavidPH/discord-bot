from discord.ext import commands
from datetime import datetime
import discord as d
import asyncio
import typing


def check_admin():
    async def pred(ctx):
        return ctx.author.guild_permissions.administrator or await ctx.cog.client.is_owner(ctx.author)

    return commands.check(pred)


class HelpCommands(commands.HelpCommand):
    def __init__(self):
        super().__init__(command_attrs={
            'help': 'Shows help about the bot, a command, or a category',
            'usage': '[command|category]',
            'cooldown': commands.Cooldown(1, 3.0, commands.BucketType.member)})

    async def command_callback(self, ctx, *, command=None):
        if command:
            cog = ctx.bot.get_cog(command.title())
            if cog is not None and cog.qualified_name in ctx.bot.initial_cogs:
                return await self.send_cog_help(cog)
        await super().command_callback(ctx, command=command)

    async def send_bot_help(self, mapping):
        ctx = self.context
        client = ctx.bot
        loaded_cogs = client.cogs
        cogs = [cog for cog in client.initial_cogs if cog in loaded_cogs]
        dest = self.get_destination()
        # Send Messages and add reactions
        embed = await self.get_cog_page(0, cogs)
        help_embed = await dest.send(embed=embed)
        client.loop.create_task(self.add_reactions(help_embed))

        # Listen for reactions
        await self.paginator_wait(ctx, client, help_embed, cogs)

    async def send_cog_help(self, cog):
        ctx = self.context
        client = ctx.bot
        loaded_cogs = client.cogs
        cogs = [cog for cog in client.initial_cogs if cog in loaded_cogs]
        page = cogs.index(cog.qualified_name) + 1
        dest = self.get_destination()
        # Send Messages and add reactions
        embed = await self.get_cog_page(page, cogs)  # Remove qualified name thing
        help_embed = await dest.send(embed=embed)
        client.loop.create_task(self.add_reactions(help_embed))

        # Listen for reactions
        await self.paginator_wait(ctx, client, help_embed, cogs, page)

    async def send_group_help(self, group):
        if group.cog.qualified_name not in self.context.bot.initial_cogs:
            return
        if group.hidden:
            return
        dest = self.get_destination()
        embed = self.make_command_help(group, True)
        await dest.send(embed=embed)

    async def send_command_help(self, command):
        if command.cog.qualified_name not in self.context.bot.initial_cogs:
            return
        if command.hidden:
            return
        dest = self.get_destination()
        embed = self.make_command_help(command, False)
        await dest.send(embed=embed)

    def make_command_help(self, command, isgroup):
        ctx = self.context
        client = ctx.bot
        cog_name = command.cog.qualified_name
        if command.parent:
            name = f"{command.parent} > {command.name}"
        else:
            name = command.name
        embed = d.Embed(title=f"{cog_name} > {name}",
                        color=client.embed_color)
        if command.aliases:
            embed.add_field(name="Aliases:", value=" ".join(f"`{a}`" for a in command.aliases))

        embed.add_field(name="Usage", value=f"**{self.get_command_signature(command)}**\n\n"
                                            " `[]`  optional argument.\n"
                                            "`<>` required argument.\n"
                                            "\u200b \u200b `|` mutually exclusive arguments.", inline=False)
        if isgroup:
            subcommands = [f"`{self.clean_prefix}{c.qualified_name}`" for c in command.walk_commands() if not c.hidden]
            subcommands = list(dict.fromkeys(subcommands))
            if subcommands:
                embed.add_field(name="Subcommands", value="\n".join(subcommands))

        if command.help:
            description = self.get_command_help(command)
        else:
            description = self.get_command_description(command)

        embed.description = description
        embed.set_footer(text=f"To view all commands in this category type {self.clean_prefix}help {cog_name}")
        return embed

    def get_command_indented_signature(self, command):
        if not command.signature and not command.parent:  # checking if it has no args and isn't a subcommand
            return f'{self.clean_prefix}{command.name}\t'
        if command.signature and not command.parent:  # checking if it has args and isn't a subcommand
            return f'{self.clean_prefix}{command.name}\t{command.signature}'
        if not command.signature and command.parent:  # checking if it has no args and is a subcommand
            return f' * {command.name}'
        else:  # else assume it has args a signature and is a subcommand
            return f' * {command.name}\t{command.signature}'

    def get_command_signature(self, command):
        if not command.signature and not command.parent:  # checking if it has no args and isn't a subcommand
            return f'`{self.clean_prefix}{command.name}`\t'
        if command.signature and not command.parent:  # checking if it has args and isn't a subcommand
            return f'`{self.clean_prefix}{command.name}` `{command.signature}`'
        if not command.signature and command.parent:  # checking if it has no args and is a subcommand
            return f'`{self.clean_prefix}{command.parent} {command.name}`'
        else:  # else assume it has args a signature and is a subcommand
            return f'`{self.clean_prefix}{command.parent} {command.name}` `{command.signature}`'

    def get_command_description(self, command):
        if not command.short_doc:
            return ""
        else:
            return command.short_doc.replace("{prefix}", self.clean_prefix)

    def get_command_help(self, command):
        if not command.help:
            return ""
        else:
            return command.help.replace("{prefix}", self.clean_prefix)

    @staticmethod
    async def add_reactions(msg):
        reactions = ('⏮', '◀', 'ℹ', '⏹', '▶', '⏭')
        for reaction in reactions:
            await msg.add_reaction(reaction)

    async def paginator_wait(self, ctx, client, message, cogs, page=0):
        show_info = False

        def check(r, u):
            return u == ctx.author

        while True:
            try:
                reaction, user = await client.wait_for("reaction_add", timeout=60.0, check=check)
            except asyncio.TimeoutError:
                try:
                    await message.clear_reactions()
                except d.errors.Forbidden:
                    pass
                break
            else:
                try:
                    await message.remove_reaction(str(reaction.emoji), ctx.author)
                except d.errors.Forbidden:
                    pass
                page_total = len(cogs) + 1
                if str(reaction.emoji) == '⏭':  # go to the last the page
                    page = page_total - 1
                    embed = await self.get_cog_page(page, cogs, show_info)
                    await message.edit(embed=embed)
                elif str(reaction.emoji) == '⏮':  # go to the first page
                    page = 0
                    embed = await self.get_cog_page(page, cogs, show_info)
                    await message.edit(embed=embed)
                elif str(reaction.emoji) == '◀':  # go to the previous page
                    page -= 1
                    if page == -1:  # check whether to go to the final page
                        page = page_total - 1
                    embed = await self.get_cog_page(page, cogs, show_info)
                    await message.edit(embed=embed)
                elif str(reaction.emoji) == '▶':  # go to the next page
                    page += 1
                    if page == page_total:  # check whether to go to the first page
                        page = 0
                    embed = await self.get_cog_page(page, cogs, show_info)
                    await message.edit(embed=embed)
                elif str(reaction.emoji) == '⏹':  # delete the message and break from the wait_for
                    await message.delete()
                    break
                if str(reaction.emoji) == 'ℹ' and page != 0:
                    show_info = not show_info
                    embed = await self.get_cog_page(page, cogs, show_info)
                    await message.edit(embed=embed)

    async def first_page(self):
        ctx = self.context
        client = ctx.bot
        embed = d.Embed(color=client.embed_color)
        embed.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
        embed.description = client.description + "\n```diff\n" \
                                                 "- [] optional argument\n" \
                                                 "- <> required argument\n" \
                                                 "- Do NOT type these when using commands.\n" \
                                                 f"+ Type {self.clean_prefix}help [command | category] for " \
                                                 "more help on a command or category.\n" \
                                                 "+ Clicking [ℹ] swaps between arguments / description view.\n```" \
                                                 "[Invite link](https://discord.com/api/oauth2/" \
                                                 f"authorize?client_id={client.user.id}" \
                                                 "&permissions=277361454272&scope=bot+applications.commands) | [Contact](https://dsc.bio/xain)" \
                                                 " | [Support Server](https://discord.gg/XZkUPTD)"

        news_channel = client.get_channel(client.channels["latest"])
        last_news_embed = (await news_channel.history(limit=1).flatten())[0].embeds[0]
        cogs = [f"\u2022 {cog}" for cog in client.initial_cogs if cog in client.cogs]
        embed.add_field(name="Categories:", value="\n".join(cogs))
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name=last_news_embed.title,
                        value=f"{last_news_embed.description.splitlines()[0]}\n"
                              f"Type `{self.clean_prefix}latest` for more.")
        embed.add_field(name="Send feedback!", value=f"Any feedback/suggestions/bug reports are greatly appreciated.\n"
                                                     f"Send them to Xain#6873, or by using the "
                                                     f"`{self.clean_prefix}feedback` command", inline=False)
        return embed

    async def get_cog_page(self, page: int, cogs, info=False):
        if page == 0:
            return await self.first_page()
        else:
            page -= 1
        ctx = self.context
        client = ctx.bot
        cog = client.get_cog(cogs[page])  # get the current cog
        embed = d.Embed(title=f"{cog.qualified_name}",
                        description=f"{cog.description.replace('{prefix}', self.clean_prefix)}",
                        color=client.embed_color)
        embed.set_author(name=f'Category {page + 1}/{len(cogs)}', icon_url=ctx.author.avatar_url)
        comds = [c for c in cog.walk_commands() if not c.hidden]
        comds = list(dict.fromkeys(comds))  # discord.py bug of giving duplicates, will be fixed in 1.4
        descr = [self.get_command_description(c) for c in comds]
        all_parents = not any([c for c in comds if c != comds[0] and c.parent != comds[0]])
        if all_parents:
            tabsize = 11 + len(self.clean_prefix)
        else:
            tabsize = max(self.get_max_size(comds) + len(self.clean_prefix) + 3, 8)
        if info:
            selected = "ℹ (parameters) **`(info)`**"
        else:
            selected = '**`[optional]`  `<required>`  `mutually|exclusive`**\n'
            selected += "\nℹ**`(parameters)`** (info)"

        text = f"\n\n{selected}\n```\n"
        for i, c in enumerate(comds):
            if not info:
                text += self.get_command_indented_signature(c).expandtabs(tabsize) + "\n"
            else:
                if c.parent:
                    text += f" * {c.name}\t{descr[i]}\n".expandtabs(tabsize)
                else:
                    text += f"{self.clean_prefix}{c.name}\t{descr[i]}\n".expandtabs(tabsize)

        text += "\n```"
        embed.description += text
        embed.set_footer(text=f"Type {self.clean_prefix}help [command_name] for more info on a specific command.")
        return embed


class Settings(commands.Cog):
    """
        Bot-wide configuration commands.
    """

    def __init__(self, client):
        self.client = client
        self.color = client.embed_color
        self.client.help_command = HelpCommands()
        self.client.help_command.cog = self

    @commands.is_owner()
    @commands.command(hidden=True)
    async def shutdown(self, ctx):
        await ctx.send("Shutting down.")
        await self.client.close()

    @commands.is_owner()
    @commands.command(hidden=True)
    async def load(self, ctx, cog):
        cog = cog.title()
        self.client.load_extension(f'cogs.{cog}')
        await ctx.send(f":inbox_tray: `cogs.{cog}`")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def unload(self, ctx, cog):
        cog = cog.title()
        self.client.unload_extension(f'cogs.{cog}')
        await ctx.send(f":outbox_tray: `cogs.{cog}`")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def reload(self, ctx, cog):
        cog = cog.title()
        self.client.unload_extension(f'cogs.{cog}')
        self.client.load_extension(f'cogs.{cog}')
        await ctx.send(f":repeat: `cogs.{cog}`")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def delete(self, ctx, amount=100):
        channel = ctx.message.channel
        msgs = []
        async for message in channel.history(limit=amount):
            time_between_insertion = datetime.now() - message.created_at
            if time_between_insertion.days < 14:
                msgs.append(message)
        await channel.delete_messages(msgs)

    @check_admin()
    @commands.command(usage="<member|role>")
    @commands.guild_only()
    async def allow(self, ctx, sf: typing.Union[d.Role, d.Member]):
        """
        Give bot permissions to someone or a role.
        If this is not set, bot will check for administrator rights, where required.
        """
        parent = ctx.command.root_parent
        async with self.client.pool.acquire() as con:
            async with con.transaction():
                if parent is None:
                    await con.execute("""
                    INSERT INTO permissions 
                        VALUES ($1, $2, $3, $3)
                    ON CONFLICT (server_id, snowflake_id) 
                        DO UPDATE
                          SET jail = $3,
                              signup = $3
                    """, ctx.guild.id, sf.id, True)
                elif str(parent).lower() == "jail":
                    await con.execute("""
                    INSERT INTO permissions(server_id, snowflake_id, jail)
                        VALUES ($1, $2, $3)
                    ON CONFLICT (server_id, snowflake_id) 
                        DO UPDATE
                          SET jail = $3
                    """, ctx.guild.id, sf.id, True)
                elif str(parent).lower() == "signup":
                    await con.execute("""
                    INSERT INTO permissions(server_id, snowflake_id, signup)
                        VALUES ($1, $2, $3)
                    ON CONFLICT (server_id, snowflake_id) 
                        DO UPDATE
                          SET signup = $3
                    """, ctx.guild.id, sf.id, True)

        await ctx.send(f"Gave {type(sf).__name__}: '{sf.name}' permissions to use {parent or 'jail and signup'}.")

    @check_admin()
    @commands.command(usage="<member|role>")
    @commands.guild_only()
    async def deny(self, ctx, sf: typing.Union[d.Role, d.Member]):
        """
        Blacklist a person or a role from jail / signup.
        If someone is blacklisted, they will not be able to use jail or signup no matter what.
        """
        parent = ctx.command.root_parent
        async with self.client.pool.acquire() as con:
            async with con.transaction():
                if parent is None:
                    await con.execute("""
                            INSERT INTO permissions 
                                VALUES ($1, $2, $3, $3)
                            ON CONFLICT (server_id, snowflake_id) 
                                DO UPDATE
                                  SET jail = $3,
                                      signup = $3
                            """, ctx.guild.id, sf.id, False)
                elif str(parent).lower() == "jail":
                    await con.execute("""
                            INSERT INTO permissions(server_id, snowflake_id, jail)
                                VALUES ($1, $2, $3)
                            ON CONFLICT (server_id, snowflake_id) 
                                DO UPDATE
                                  SET jail = $3
                            """, ctx.guild.id, sf.id, False)
                elif str(parent).lower() == "signup":
                    await con.execute("""
                            INSERT INTO permissions(server_id, snowflake_id, signup)
                                VALUES ($1, $2, $3)
                            ON CONFLICT (server_id, snowflake_id) 
                                DO UPDATE
                                  SET signup = $3
                            """, ctx.guild.id, sf.id, False)

        await ctx.send(f"Denied {type(sf).__name__}: '{sf.name}' permissions to use {parent or 'jail and signup'}.")

    @commands.command(usage="<member|role>")
    @check_admin()
    @commands.guild_only()
    async def remove(self, ctx, sf: typing.Union[d.Role, d.Member]):
        """
        Remove a member or a role's permissions.
        If you want to deny someone the ability to use a permission use {prefix}deny instead.
        """
        async with self.client.pool.acquire() as con:
            async with con.transaction():
                x = await con.execute("DELETE FROM permissions WHERE server_id = $1 AND snowflake_id = $2",
                                      ctx.guild.id, sf.id)
                if x == "DELETE 0":
                    await ctx.send("User did not have any permissions set.")
                else:
                    await ctx.send(f"Removed {type(sf).__name__}: '{sf.name}' permissions")

    @commands.command()
    @commands.guild_only()
    async def perms(self, ctx):
        """
        View saved permissions.
        Use `{prefix}jail <allow | deny> <member | role>`
        or `{prefix}signup <allow | deny> <member | role>`
        To set command specific permission.
        Or use `{prefix}<allow | deny> <member | role>` to set both at once.
        To remove a permission use `{prefix} remove <member | role>`

        A denied permission will always overrule any allow.
            E.g:
                <user> has a role that lets them use {prefix}jail
                You use `{prefix}jail deny <user>`
                They will not be able to use `{prefix}jail`

        """
        parent = ctx.command.root_parent
        embed = d.Embed(color=self.client.embed_color)
        async with self.client.pool.acquire() as con:
            perms = await con.fetch("SELECT * FROM permissions WHERE server_id = $1", ctx.guild.id)

        perm_dict = {"jail": [], "signup": []}
        bool_str = {True: "allowed", False: "denied"}
        for record in perms:
            name = ctx.guild.get_role(record["snowflake_id"]) or \
                   ctx.guild.get_member(record["snowflake_id"]) or \
                   record["snowflake_id"]
            if record["jail"] is not None:
                perm_dict["jail"].append(f"{name} - {bool_str[record['jail']]}")
            if record["signup"] is not None:
                perm_dict["signup"].append(f"{name} - {bool_str[record['signup']]}")

        if parent is None:
            keys = perm_dict.keys()
        else:
            keys = [str(parent).lower()]
        for key in keys:
            if perm_dict[key]:
                embed.add_field(name=f"**{key.capitalize()}**",
                                value="```\n" + "\n".join(perm_dict[key]) + "\n```",
                                inline=False)
            else:
                embed.add_field(name=f"**{key.capitalize()}**", value="```\n<Empty>\n```", inline=False)

        embed.set_footer(text=f"See {ctx.prefix}help perms for info on how permissions work.")
        await ctx.send(embed=embed)

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def prefix(self, ctx):
        """
        Shows the current's server prefix.
        """
        async with self.client.pool.acquire() as con:
            prefix = await con.fetchval("SELECT server_prefix FROM servers WHERE server_id = $1", ctx.guild.id)

        if prefix:
            self.client.guild_prefixes[ctx.guild.id] = prefix
        await ctx.send(f"This server's prefix is `{' '.join(prefix or self.client.default_prefix)}`\n"
                       f"You can also always use commands by tagging me.")

    @check_admin()
    @prefix.command(usage="<new_prefix>", name="set")
    async def set_prefix(self, ctx, prefix):
        """
        Changes the current server's prefix.
        If you want to use spaces write the prefix surrounded by quotes "like this".
        """
        async with self.client.pool.acquire() as con:
            async with con.transaction():
                await con.execute("UPDATE servers SET server_prefix = $1 WHERE server_id = $2", [prefix], ctx.guild.id)

        self.client.guild_prefixes[ctx.guild.id] = [prefix]
        await ctx.send(f"Server's prefix set to `{prefix}`\n"
                       f"You can also always use commands by tagging me.")

    @commands.command(usage="<message>")
    async def feedback(self, ctx, *, message):
        """
        Send some feedback!
        Any suggestion, feature request, or bug report is very appreciated.
        This will forward the message to the bot's feedback channel. I'll get back to you about it if needed.
        Alternatively, you can send a pm to Xain#6873
        """
        embed = d.Embed(color=self.color)
        embed.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
        embed.add_field(name="Display Name", value=ctx.author.display_name)
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name="Author ID", value=ctx.author.id)

        embed.add_field(name="Channel Name", value=str(ctx.channel))
        embed.add_field(name="\u200b", value="\u200b")
        embed.add_field(name="Channel ID", value=ctx.channel.id)
        embed.add_field(name="Message: ", value=message, inline=False)
        feedback_ch = self.client.get_channel(self.client.channels["feedback"])
        await ctx.message.add_reaction("\U0001f44d")
        await ctx.send("Feedback sent, thank you!")
        await feedback_ch.send(embed=embed)

    @commands.group(invoke_without_command=True, hidden=True)
    async def latest(self, ctx):
        """
        Shows the bot's latest news / update.
        """
        news_channel = self.client.get_channel(self.client.channels["latest"])
        last_msg = (await news_channel.history(limit=1).flatten())[0]
        embed = last_msg.embeds[0].set_footer(text=f"For a full list of commands type {ctx.prefix}help")
        await ctx.send(embed=embed)

    @latest.command(hidden=True)
    @commands.is_owner()
    async def make(self, ctx, *, description=None):

        news_date = datetime.now().strftime("%b %d, %Y")
        embed = d.Embed(color=self.color, title=f"Latest News - <:news:728706669617610832> {news_date}")
        embed.set_author(name=self.client.user.display_name, icon_url=self.client.user.avatar_url)
        embed.description = description
        await ctx.message.delete()
        await ctx.send(embed=embed)


# Load Cog

def setup(client):
    admin_cog = Settings(client)
    client.add_cog(admin_cog)
