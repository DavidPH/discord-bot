import asyncpg


class Database:
    @classmethod
    async def create_pool(cls, config):
        host = config["host"]
        port = config["port"]
        user = config["user"]
        password = config["password"]
        pool = await asyncpg.create_pool(user=user, password=password,
                                         host=host, port=port)
        await cls.create_tables(pool)
        return pool

    @classmethod
    async def create_tables(cls, pool):
        async with pool.acquire() as con:
            async with con.transaction():
                await con.execute(
                    ("""
                        CREATE TABLE IF NOT EXISTS "servers"
                        (
                         "server_id"     bigint NOT NULL,
                         "server_name"   text NOT NULL,
                         "server_prefix" text[] NULL,
                         CONSTRAINT "PK_server" PRIMARY KEY ( "server_id" )
                        );
                        
                        CREATE TABLE IF NOT EXISTS "permissions"
                        (
                            server_id bigint NOT NULL,
                            snowflake_id bigint NOT NULL,
                            jail boolean NULL,
                            signup boolean NULL,
                            PRIMARY KEY (snowflake_id, server_id),
                            FOREIGN KEY (server_id)
                                REFERENCES "servers" (server_id)
                                ON UPDATE CASCADE
                                ON DELETE CASCADE
                        );
                        
                        CREATE TABLE IF NOT EXISTS "jail_settings"
                        (
                            server_id bigint NOT NULL,
                            jail_message text NULL DEFAULT 'You have been jailed in {server}\nDuration: {time_left}',
                            banned_words json NULL,
                            jail_roles bigint[] NOT NULL, 
                            PRIMARY KEY (server_id),
                            FOREIGN KEY (server_id)
                                REFERENCES "servers" (server_id)
                                ON UPDATE CASCADE
                                ON DELETE CASCADE 
                        );
                        
                        CREATE TABLE IF NOT EXISTS "jail"
                        (
                            server_id bigint NOT NULL,
                            member_id bigint NOT NULL,
                            jailed_by bigint NOT NULL,
                            role_list bigint[] NOT NULL,
                            jailed_at timestamp NOT NULL,
                            release_at timestamp NULL,
                            PRIMARY KEY (server_id, member_id),
                            FOREIGN KEY (server_id)
                                REFERENCES "servers" (server_id)
                                ON UPDATE CASCADE 
                                ON DELETE CASCADE
                        );
                        
                        CREATE TABLE IF NOT EXISTS "signup_settings"
                        (
                            server_id bigint NOT NULL,
                            format_name text NOT NULL,
                            format json NOT NULL,
                            PRIMARY KEY (server_id, format_name),
                            FOREIGN KEY (server_id)
                                REFERENCES "servers" (server_id)
                                ON UPDATE CASCADE 
                                ON DELETE CASCADE
                        );
                        
                        CREATE TABLE IF NOT EXISTS "signups"
                        (
                            server_id bigint NOT NULL,
                            message_id bigint[] NOT NULL,
                            content json NOT NULL,
                            timestamp timestamp NULL DEFAULT current_timestamp,
                            PRIMARY KEY (server_id, message_id),
                            FOREIGN KEY (server_id)
                                REFERENCES "servers" (server_id)
                                ON UPDATE CASCADE 
                                ON DELETE CASCADE
                        );
                         
                    """)
                )
