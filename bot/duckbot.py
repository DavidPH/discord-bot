from discord.ext import commands
import discord as d
import aiohttp
import asyncio
import os


async def _prefix_callable(bot, msg=None):
    user_id = bot.user.id
    base = [f'<@!{user_id}> ', f'<@{user_id}> ']
    server = msg.guild
    if msg and server is None:
        base.extend(bot.default_prefix)
    else:
        if server.id not in bot.guild_prefixes:
            async with bot.pool.acquire() as con:
                prefixes = await con.fetchval("SELECT server_prefix FROM servers WHERE server_id = $1", server.id)
            bot.guild_prefixes[server.id] = prefixes
        else:
            prefixes = bot.guild_prefixes[server.id]
        base.extend(prefixes or bot.default_prefix)

    return base


class DuckBot(commands.AutoShardedBot):
    def __init__(self, pool, channels, dirpath):
        intents = d.Intents.default()
        intents.members = True
        super().__init__(command_prefix=_prefix_callable, case_insensitive=True, intents=intents)
        self.embed_color = 0xfef200
        self.pool = pool
        self.session = aiohttp.ClientSession(loop=self.loop)
        self.default_prefix = [","]
        self.guild_prefixes = {}
        self.channels = channels  # feedback and latest channel ID's
        self.dirpath = dirpath  # run.py directory
        # Only show these in the help command
        self.initial_cogs = ["Moderation", "Events", "Fun", "Settings"]

        extensions = [f"cogs.{cog}" for cog in self.initial_cogs]
        extensions.extend([f"cogs.{e[:-3]}" for e in os.listdir(self.dirpath / "cogs") if
                           e.endswith(".py") and e[:-3] not in self.initial_cogs])
        extensions.extend(["jishaku"])
        os.environ["JISHAKU_HIDE"] = "true"  # Prevents jishaku from showing up in help command
        for extension in extensions:
            try:
                self.load_extension(extension)
                print(f"Loaded {extension}")
            except Exception as e:
                print(f"Failed to load extension {extension}")
                print(e)

    def default_embed(self, ctx):
        embed = d.Embed(color=self.embed_color)
        embed.set_footer(text=f"For a full set of commands type {ctx.prefix}help")
        return embed

    async def on_ready(self):
        print(f'Logged in as {self.user}')
        await self.change_presence(activity=d.Activity(type=d.ActivityType.playing, name="See ,help"))

        async with self.pool.acquire() as con:
            server_records = await con.fetch("SELECT server_id, server_name FROM servers")
            servers = []
            left_servers = []
            name_update = []
            for record in server_records:
                guild = self.get_guild(record["server_id"])
                if guild:
                    servers.append(guild)
                    if guild.name != record["server_name"]:
                        name_update.append([guild.name, guild.id])
                else:
                    left_servers.append([record["server_id"]])
            missing = [[server.id, str(server)] for server in self.guilds if server not in servers]
            async with con.transaction():

                # Add servers bot is in that are not in the db
                await con.executemany("INSERT INTO servers (server_id, server_name) VALUES ($1, $2)", missing)

                # Update server names if they changed
                await con.executemany("UPDATE servers SET server_name = $1 WHERE server_id = $2", name_update)

                # Remove servers bot is no longer in
                await con.executemany("DELETE FROM servers WHERE server_id = $1", left_servers)

    async def on_guild_join(self, guild):
        async with self.pool.acquire() as con:
            async with con.transaction():
                if not await con.fetchval("SELECT server_id FROM servers WHERE server_id = $1", guild.id):
                    await con.execute("INSERT INTO servers (server_id, server_name) VALUES ($1, $2)", guild.id,
                                      str(guild))

    async def on_guild_remove(self, guild):
        async with self.pool.acquire() as con:
            async with con.transaction():
                await con.execute("DELETE FROM servers WHERE server_id = $1", guild.id)

    async def on_message(self, message):
        if message.author == self.user:
            return
        await self.process_commands(message)

    async def on_command_error(self, ctx, error):
        if hasattr(ctx.command, "on_error"):
            return
        if isinstance(error, commands.CommandNotFound):
            return
        try:
            if isinstance(error, commands.NoPrivateMessage):
                await ctx.author.send('This command cannot be used in private messages.')
                return
            elif isinstance(error, commands.DisabledCommand):
                await ctx.author.send('Sorry. This command is disabled and cannot be used.')
                return
        except d.Forbidden:
            return
        if isinstance(error, commands.MissingPermissions) or isinstance(error, commands.CheckFailure):
            await ctx.send("You do not have permissions to do this.")
            return
        embed = self.default_embed(ctx)
        if isinstance(error, commands.UserInputError):
            signature = ctx.command.signature
            val = ""
            if signature:
                val += f"**Correct usage**: \n" \
                       f"`{ctx.prefix}{ctx.command} {signature}`\n\n"
            val += f"For more info type `{ctx.prefix}help {ctx.command}`"
            embed.add_field(name="Invalid arguments", value=val)
        else:
            embed.add_field(name="Error:", value=error)

        await ctx.send(embed=embed)

    async def close(self):
        await self.pool.close()
        await self.session.close()
        [task.cancel() for task in asyncio.all_tasks(loop=self.loop)]
        await super().close()
