import os
import discord as d
import asyncio
import logging
import configparser
import argparse
from pathlib import Path
from db import Database
from duckbot import DuckBot


def setup_logging():
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)


def main():
    # Set up bot config
    bot_dir = Path(__file__).parent
    config_path = bot_dir / "resources/config/bot.config"
    config = configparser.ConfigParser()
    if not config.read(config_path):
        print("No config file")
        exit(-1)
    parser = argparse.ArgumentParser(description="Start Duckbot")
    parser.add_argument('-t', '--token', metavar='', type=str, help='set discord api token')
    parser.add_argument('-db', '--database', metavar='', type=str, help='DB credentials in the format of '
                                                                        'user:password@host:port')
    parser.add_argument('-l', '--latest', metavar='', type=int, help='set the latest channel')
    parser.add_argument('-f', '--feedback', metavar='', type=int, help='set the feedback channel')
    parser.add_argument('-s', '--stop', action="store_true", default=False,
                        help='exits after set up instead of running the bot')
    args = parser.parse_args()
    if token := os.environ.get("TOKEN", args.token):
        config["DEFAULT"]["token"] = token
    if db := os.environ.get("DATABASE", args.database):
        cred, host = db.split("@")
        cred = cred.split(":")
        host = host.split(":")
        config["POSTGRESQL"]["user"] = cred[0]
        config["POSTGRESQL"]["password"] = cred[1]
        config["POSTGRESQL"]["host"] = host[0]
        config["POSTGRESQL"]["port"] = host[1]
    if latest := os.environ.get("LATEST", args.latest):
        config["DEFAULT"]["latest"] = str(latest)
    if feedback := os.environ.get("FEEDBACK", args.feedback):
        config["DEFAULT"]["feedback"] = str(feedback)
    with open(config_path, 'w') as config_file:
        config.write(config_file)
    if args.stop:
        exit(0)
    loop = asyncio.get_event_loop()
    pool = loop.run_until_complete(Database.create_pool(config["POSTGRESQL"]))
    bot = DuckBot(pool,
                  channels={"latest": int(config["DEFAULT"]["latest"]), "feedback": int(config["DEFAULT"]["feedback"])}
                  , dirpath=bot_dir)
    try:
        bot.run(config["DEFAULT"]["Token"])
    except d.LoginFailure:
        print("Invalid token configured.\n"
              f"Check {config_path}")
    finally:
        print("Shutting down.")


if __name__ == '__main__':
    main()
